import utilities.connect as dbc
import pyperclip as clip

prod_dw = dbc.server_connect('azprod-bi-sql', 'SpreeDW')

vc_column_structure_query = """SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = 'fact' AND
      TABLE_NAME = 'ValueChain'"""

vc_column_structure = dbc.query(prod_dw, vc_column_structure_query)

print(vc_column_structure)

select_block = ''
where_block = 'WHERE 1=1 AND (\n'

for index, column_name in vc_column_structure.iterrows():
    column = column_name['COLUMN_NAME']
    print(column)
    select_block += f'\tlocal.{column} {column}_local, \n'
    select_block += f'\tprod.{column} {column}_prod, \n'
    if column not in ['FactValueChainID', 'CreatedBy', 'CreatedOn', 'Partition']:
        where_block += f'\tlocal.{column} != prod.{column} OR\n'

where_block = where_block[:len(where_block) - 4] + '\n)'
select_block = select_block[:len(select_block) - 3]
output_query = f"""SELECT 
{select_block}
FROM SpreeDW.fact.ValueChain local
LEFT OUTER JOIN [AZPROD-BI-SQL].SpreeDW.fact.ValueChain prod ON prod.PackageItemID = local.PackageItemID
{where_block}"""

print(output_query)
clip.copy(output_query)


