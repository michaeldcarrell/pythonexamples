import utilities.connect as dbc

spreedw = dbc.server_connect('localhost', 'SpreeDW')

dim_tables_query = """SELECT
    TABLE_SCHEMA,
    TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'dim'"""

dim_tables = dbc.query(spreedw, dim_tables_query)

truncate_statement = "TRUNCATE TABLE {}.{};"

for index, table in dim_tables.iterrows():
    dbc.statement(spreedw, truncate_statement.format(table['TABLE_SCHEMA'], table['TABLE_NAME']))
