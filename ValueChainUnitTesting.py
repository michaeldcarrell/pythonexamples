import utilities.connect as dbc
import pandas as pd
import sqlalchemy as sql
import warnings
import numpy as np
import datetime
start_time = datetime.datetime.now()
print(start_time)
warnings.simplefilter(action='ignore', category=UserWarning)

prod_engine = sql.create_engine("mssql+pyodbc://azint-bi-sql/SpreeDW?trusted_connection=yes&driver=SQL+Server+Native+Client+11.0")
local_engine = sql.create_engine("mssql+pyodbc://localhost/Validation?trusted_connection=yes&driver=SQL+Server+Native+Client+11.0")

local_dw = dbc.server_connect('localhost', 'SpreeDW')
prod_dw = dbc.server_connect('azint-bi-sql', 'SpreeDW')

local_vc = dbc.query(local_dw, "SELECT * FROM fact.ValueChain")

# Create temp table with all the pkgitemids in it to join to the prod fact table
local_vc_pkgitems = pd.DataFrame(local_vc['PackageItemID'])
local_vc_pkgitems.to_sql('#UnitTest', prod_engine, if_exists='replace', index=False)

where_block = 'WHERE PackageItemID IN (\n'

for item in local_vc['PackageItemID']:
    if item != 0:
        where_block += f'\t{item}, \n'
where_block = where_block[:len(where_block) - 3] + '\n)'
prod_query = f"""SELECT
    *
FROM fact.ValueChain
{where_block}"""
# prod_query = """SELECT vc.*
# FROM fact.ValueChain vc
# JOIN #UnitTest test ON vc.PackageItemID = test.PackageItemID"""
prod_vc_values = dbc.query(prod_engine, prod_query)
val_diff_cols = ['PackageItemID', 'Column', 'LocalValue', 'ProdValue']
value_difference = pd.DataFrame(columns=val_diff_cols)

column_diff_exception = ['PackageItemID', 'FactValueChainID', 'CreatedOn', 'CreatedBy', 'ProcessedOriginalSale']

for row_index, pkgitem in prod_vc_values.iterrows():
    current_packageitemid = pkgitem.PackageItemID
    prod_vc_record = pd.DataFrame(pkgitem)
    prod_vc_record_test_cols = prod_vc_record[~prod_vc_record.index.isin(column_diff_exception)]
    for col_index, prod_col_record in prod_vc_record_test_cols.iterrows():
        if prod_col_record.reset_index(drop=True)[0] != local_vc[local_vc.PackageItemID == current_packageitemid][col_index].reset_index(drop=True)[0]:
            current_diffs = pd.DataFrame(
                np.array([
                    current_packageitemid,
                    col_index,
                    local_vc[local_vc.PackageItemID == current_packageitemid][col_index].reset_index(drop=True)[0],
                    prod_col_record.reset_index(drop=True)[0]
                ]).reshape(1, 4),
                columns=val_diff_cols
            )
            value_difference = value_difference.append(current_diffs)

value_difference = value_difference.reset_index(drop=True)
value_difference.to_sql(name='ValueChainUnitTest', schema='validation', con=local_engine, if_exists='replace', index=False)
end_time = datetime.datetime.now()
print(end_time)
print(f'Elapsed Time: {end_time - start_time}')

