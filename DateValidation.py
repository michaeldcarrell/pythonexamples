import utilities.connect as dbc
import pandas as pd

local_dw = dbc.server_connect('localhost', 'SpreeDW')
prod_dw = dbc.server_connect('azprod-bi-sql', 'SpreeDW')

prod_query = """SELECT
    Date,
    Week,
    WeekPreviousWeek,
    WeekPreviousMonth,
    WeekPreviousQuarter,
    WeekPreviousYear,
    FiscalWeek,
    FiscalWeekPreviousFiscalWeek,
    FiscalWeekPreviousFiscalQuarter,
    FiscalWeekPreviousFiscalYear,
    ISOWeek,
    ISOWeekPreviousWeek,
    ISOWeekPreviousMonth,
    ISOWeekPreviousQuarter,
    ISOWeekPreviousYear,
    Quarter,
    QuarterPreviousWeek,
    QuarterPreviousMonth,
    QuarterPreviousQuarter,
    FiscalQuarter,
    FiscalQuarterPreviousFiscalWeek,
    FiscalQuarterPreviousFiscalQuarter,
    FiscalQuarterPreviousFiscalYear,
    YearWeek,
    WeekdayName
FROM dim.Date
WHERE DimDateID NOT IN (0, -1)"""

local_query = """SELECT
    Date,
    CAST(REPLACE(REPLACE(Week, 'W', ''), 'Q', '') AS INT)Week,
    CAST(REPLACE(REPLACE(WeekPreviousWeek, 'W', ''), 'Q', '') AS INT)WeekPreviousWeek,
    CAST(REPLACE(REPLACE(WeekPreviousMonth, 'W', ''), 'Q', '') AS INT)WeekPreviousMonth,
    CAST(REPLACE(REPLACE(WeekPreviousQuarter, 'W', ''), 'Q', '') AS INT)WeekPreviousQuarter,
    CAST(REPLACE(REPLACE(WeekPreviousYear, 'W', ''), 'Q', '') AS INT)WeekPreviousYear,
    CAST(REPLACE(REPLACE(FiscalWeek, 'W', ''), 'Q', '') AS INT)FiscalWeek,
    CAST(REPLACE(REPLACE(FiscalWeekPreviousFiscalWeek, 'W', ''), 'Q', '') AS INT)FiscalWeekPreviousFiscalWeek,
    CAST(REPLACE(REPLACE(FiscalWeekPreviousFiscalQuarter, 'W', ''), 'Q', '') AS INT)FiscalWeekPreviousFiscalQuarter,
    CAST(REPLACE(REPLACE(FiscalWeekPreviousFiscalYear, 'W', ''), 'Q', '') AS INT)FiscalWeekPreviousFiscalYear,
    CAST(REPLACE(REPLACE(ISOWeek, 'W', ''), 'Q', '') AS INT)ISOWeek,
    CAST(REPLACE(REPLACE(ISOWeekPreviousWeek, 'W', ''), 'Q', '') AS INT)ISOWeekPreviousWeek,
    CAST(REPLACE(REPLACE(ISOWeekPreviousMonth, 'W', ''), 'Q', '') AS INT)ISOWeekPreviousMonth,
    CAST(REPLACE(REPLACE(ISOWeekPreviousQuarter, 'W', ''), 'Q', '') AS INT)ISOWeekPreviousQuarter,
    CAST(REPLACE(REPLACE(ISOWeekPreviousYear, 'W', ''), 'Q', '') AS INT)ISOWeekPreviousYear,
    CAST(REPLACE(REPLACE(Quarter, 'W', ''), 'Q', '') AS INT)Quarter,
    CAST(REPLACE(REPLACE(QuarterPreviousWeek, 'W', ''), 'Q', '') AS INT)QuarterPreviousWeek,
    CAST(REPLACE(REPLACE(QuarterPreviousMonth, 'W', ''), 'Q', '') AS INT)QuarterPreviousMonth,
    CAST(REPLACE(REPLACE(QuarterPreviousQuarter, 'W', ''), 'Q', '') AS INT)QuarterPreviousQuarter,
    CAST(REPLACE(REPLACE(FiscalQuarter, 'W', ''), 'Q', '') AS INT)FiscalQuarter,
    CAST(REPLACE(REPLACE(FiscalQuarterPreviousFiscalWeek, 'W', ''), 'Q', '') AS INT)FiscalQuarterPreviousFiscalWeek,
    CAST(REPLACE(REPLACE(FiscalQuarterPreviousFiscalQuarter, 'W', ''), 'Q', '') AS INT)FiscalQuarterPreviousFiscalQuarter,
    CAST(REPLACE(REPLACE(FiscalQuarterPreviousFiscalYear, 'W', ''), 'Q', '') AS INT)FiscalQuarterPreviousFiscalYear
FROM dim.Date
WHERE DimDateID NOT IN (0, -1)"""

local_dim_date = dbc.query(local_dw, local_query)
prod_dim_date = dbc.query(prod_dw, prod_query)


def data_frame_compare(df1, df2, record_key):
    differences = pd.DataFrame(columns=['column', 'df1_value', 'df2_value'])

    df1_columns = df1.columns.to_list()
    df2_columns = df2.columns.to_list()
    common_columns = []
    for column in df1_columns:
        if column in df2_columns:
            common_columns.append(column)

    for column in df2_columns:
        if column in df1_columns:
            common_columns.append(column)

    for index, df1_record in df1.iterrows():
        df2_record = df2[df2[record_key[0]] == df1_record[record_key[0]]]
        df2_record.reset_index(drop=True, inplace=True)
        df2_record = df2_record.loc[0]
        for column in common_columns:
            if df1_record[column] != df2_record[column]:
                differences = differences.append(
                    pd.DataFrame(
                        data={
                            'column': [column],
                            'df1_value': [df1_record[column]],
                            'df2_value': [df2_record[column]]
                        }
                    )
                )
    differences.reset_index(drop=True, inplace=True)
    return differences


print(data_frame_compare(prod_dim_date, local_dim_date, ['Date']))
