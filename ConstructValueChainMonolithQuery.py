import pyperclip as clipboard
import argparse
import pyodbc as odbc
import pandas as pd
import datetime as dt


def server_connect(sc_server, sc_database, sc_instance=""):
    if sc_instance != "":
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server}\\{sc_instance};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )
    else:
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )

    return connection


def query(connection, sql):
    return pd.read_sql(sql, connection)


def statement(connection, sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    cursor.commit()


def load_spreetl_valuechain(where_block):
    tables = query_from_section.split('LEFT OUTER JOIN')
    tables = [table.replace('FROM ', '') for table in tables]
    tables = [table[1:len(table)] if table[0:1] == ' ' else table for table in tables]

    aliases = tables
    aliases = [alias.split(' ')[1].replace('\n', '') for alias in aliases]

    tables = [table.split(' ON ')[0] for table in tables]
    tables = [table.split(' ')[0].replace('[AZINT-BI-SQL].', '') for table in tables]

    monolith_select_template = "DROP TABLE IF EXISTS #ValueChainHolding; \nSELECT \n{}\nINTO #ValueChainHolding \n" + query_from_section

    table_structure = []

    for table in range(len(tables)):
        table_split = tables[table].split('.')
        current_database = table_split[0]
        current_schema = table_split[1]
        current_table = table_split[2]
        table_structure.append({
            "database": current_database,
            "schema": current_schema,
            "table": current_table,
            "alias": aliases[table]
        })

    monolith_select_block = ''
    table_inserts_block = ''

    for table in table_structure:
        insert_block = ''
        select_block = ''
        table_format = query(
            local_tl,
            table_format_query.format(
                table['table'],
                table['schema']
            )
        )
        current_table = table['table']
        schema = table['schema']
        database = table['database']
        key = table_format['COLUMN_NAME'][0]
        identity_table = 0
        for index, column in table_format.iterrows():
            alias = table['alias']
            col = column['COLUMN_NAME']
            identity_table += column['TABLE_IDENTITY']
            monolith_select_block += f"\t{alias}.{col} {alias}_{col}, \n"
            insert_block += f"\t{col}, \n"
            select_block += f"\t{alias}_{col}, \n"
        insert_block = insert_block[:len(insert_block) - 3]
        select_block = select_block[:len(select_block) - 3]
        current_table_block = f"""TRUNCATE TABLE {database}.{schema}.{current_table};
INSERT INTO {database}.{schema}.{current_table} (
{insert_block}
)
SELECT DISTINCT
{select_block}
FROM #ValueChainHolding
WHERE {table['alias']}_{key} IS NOT NULL;\n\n"""
        if identity_table != 0:
            current_table_block = f"""SET IDENTITY_INSERT {database}.{schema}.{current_table} ON;
{current_table_block[:len(current_table_block) - 2]}
SET IDENTITY_INSERT {database}.{schema}.{current_table} OFF;\n\n"""
        table_inserts_block += current_table_block

    monolith = monolith_select_template.format(monolith_select_block[:len(monolith_select_block) - 3])
    monolith += '\n' + where_block
    table_inserts = table_inserts_block[:len(table_inserts_block) - 2]
    spreetl_loads = monolith + '\n\n' + table_inserts
    return spreetl_loads


def dim_insert_template(linked_server, dim_database_name, dim_schema_name, dim_table_name, dim_column_block):
    local_table_address = f"{dim_database_name}.{dim_schema_name}.{dim_table_name}"
    linked_table_address = f"{linked_server}.{dim_database_name}.{dim_schema_name}.{dim_table_name}"
    return f"""SET IDENTITY_INSERT {local_table_address} ON;
TRUNCATE TABLE {local_table_address};
INSERT INTO {local_table_address} (
{dim_column_block}
)
SELECT
{dim_column_block}
FROM {linked_table_address};
SET IDENTITY_INSERT {local_table_address} OFF;"""


def load_spreedw_valuechain():
    dim_load_statement = '--Load Dims\n'
    for dim in dims_to_load:
        column_block = ''
        column_structure_query = column_structure_query_template.format(dim)
        column_structure = query(local_dw, column_structure_query)
        for index, column in column_structure.iterrows():
            column_block += f"\t{column['COLUMN_NAME']}, \n"
        column_block = column_block[:len(column_block) - 3]
        dim_load_statement += dim_insert_template('[AZINT-BI-SQL]', 'SpreeDW', 'dim', dim, column_block) + '\n\n'
    return dim_load_statement[:len(dim_load_statement) - 2]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Load ValueChain dependencies from AZINT-BI-SQL in order to test ValueChain locally")
    parser.add_argument(
        "mode",
        type=str,
        help="Choose either 'run' to execute the statement locally or 'copy' or copy the script to the clipboard"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Will print sql load script to console"
    )
    parser.add_argument(
        "-dims",
        "--loaddimtables",
        action="store_true",
        help="Will produce only statements to re-load dimension for ValueChain"
    )
    parser.add_argument(
        "-syncs",
        "--loadsynctables",
        action="store_true",
        help="Will produce only statements to re-load source sync tables for ValueChain"
    )
    args = parser.parse_args()
    if args.mode not in ['run', 'copy']:
        print('Must enter either "run" or "copy" mode. Type "python ConstructValueChainMonolithQuery.py -h" for help')
    else:
        query_from_section = """FROM [AZINT-BI-SQL].SpreeTL.btdata_dbo.Sales sale
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.dp_PackageItems dpi ON sale.SaleID = dpi.SaleID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.Purgatory.ValueChain pug ON pug.PkgItemID = dpi.PkgItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.btdata_dbo.Listings list ON list.ListingID = sale.ListingID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.btdata_dbo.Items item ON item.ItemID = dpi.ItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.btdata_dbo.Inventory inv ON inv.InventoryID = item.InventoryID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.MarketplaceRefunds markrefnd ON markrefnd.SaleID = sale.SaleID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.PayPalRefunds payplrfnd ON payplrfnd.SaleID = sale.SaleID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.stage.POSale posale ON posale.SaleID = sale.SaleID
                                                                    AND posale.ItemID = item.ItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.stage.GeneratedRMAItems rmaitems ON rmaitems.SaleID = sale.SaleID
                                                                                    AND rmaitems.ItemID = item.ItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.dp_Packages pack ON pack.PackageID = dpi.PackageID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.dp_PrintQueue printq ON printq.PackageID = pack.PackageID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.stage.FlatSalesValues salesvals ON salesvals.SaleID = sale.SaleID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.finance_costing.HistoricalFIFOCache fifo --Figure out where this gets its data from and pull that to populate the view
        ON fifo.ItemID = dpi.ItemID
        AND fifo.DateRecorded = sale.DateOnlyOfSale
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.items_ExtendedProperties itemext ON itemext.ItemID = item.ItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.items_Promotions promo ON promo.ItemID = item.ItemID AND
                                                                                 sale.DateOfSale BETWEEN
                                                                                     CASE
                                                                                         WHEN promo.DateCreated > promo.StartDate
                                                                                             THEN promo.DateCreated
                                                                                         ELSE promo.StartDate
                                                                                     END AND promo.EndDate
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.m2_dbo.dp_Adjustments adj ON adj.PkgItemID = dpi.PkgItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.stage.DetailedCOGS dcogs ON dcogs.SaleID = sale.SaleID
                                                                            AND dcogs.ItemID = item.ItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.stage.DetailedCOGS_Unshipped unshdcogs ON unshdcogs.SaleID = sale.SaleID
                                                                                        AND unshdcogs.ItemID = item.ItemID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.lookup.CleanedAddresses addr ON addr.AddressID = sale.ShippingAddressID
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.lookup.SalesChannel salechan ON salechan.SourceChannel = sale.SourceOfSale
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.lookup.ShippingCo shipco ON shipco.SourceShippingCo = pack.ShippingCo
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.lookup.VendorRebates venrebate ON venrebate.SupplierID = inv.SupplierID
                                                                                AND sale.DateOnlyOfSale >= venrebate.StartDate
                                                                                AND sale.DateOnlyOfSale <= venrebate.ProjectedEndDate
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.stage.FlatWeather_DailyForecasts weather ON weather.CallDate = sale.DateOnlyOfSale AND
                                                                                            weather.Zip = addr.Zip
        LEFT OUTER JOIN [AZINT-BI-SQL].SpreeTL.lookup.GeoCoding geo ON geo.Zip = addr.Zip"""

        dims_to_load = [
            'SalesChannel',
            'Product',
            'Geography',
            'Time',
            'SaleStatus',
            'WarehouseLocation',
            'ValueChainDatasetFilter',
            'DetailedCOGSCategory',
            'Promotion',
            'ValueChainPackageFilter'
        ]

        limiting_orders = [
            '232778156840-1658912583013',  # Sale with refund
            '112-7175695-5196267',  # sale with refunds that don't match up
            '002-9553446404',  # Vendor Rebate
            '112-0028593-8636245',  # DetailedCogs Unshipped
            '351814095516-1032489560022',  # Shipping Charged
            '109-1501186-4200227',  # GMVLoss
            '113-1282441-4162612',  # WeatherForecast
            '109-7447769-1008269',  # UnshippedItemCost
            '230974027723-1700835324013',  # Promo
            '232752163928-1656643567013',  # FIFOItemCost only
            '112-8384389-8275458',  # RemainingAdjFee
            '113-5122295-7262607',  # Positive GMV
            '233014970450-1654724190013',  # Replacement Sale
            'WM-3251965613238',  # Sale Qty > 1
            '113-5122295-7262607',  # ccRatio =1
            'WM-3251965613238',  # ccRatio < 1
            '111-6814386-9143414',  # Capcost
            '352466084153-1088973690022',  # Capcost
            'CHAD-1988009',  # Capcost
            'CHAD-2328478'  # Capcost
        ]

        where_clause_records = ''
        for order in limiting_orders:
            where_clause_records += f"\t'{order}', \n"
        where_clause = f'WHERE sale.OrderID IN (\n{where_clause_records[:len(where_clause_records) - 3]}\n);'

        column_structure_query_template = """SELECT COLUMN_NAME
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_SCHEMA = 'dim' AND
              TABLE_NAME = '{}'"""

        table_format_query = """SELECT
    all_columns.name COLUMN_NAME,
    CASE
        WHEN identity_columns.object_id IS NOT NULL THEN 1
        ELSE 0
    END TABLE_IDENTITY
FROM sys.all_columns
JOIN sys.tables ON tables.object_id = all_columns.object_id
JOIN sys.schemas ON schemas.schema_id = tables.schema_id
LEFT OUTER JOIN sys.identity_columns ON identity_columns.object_id = tables.object_id AND
                             identity_columns.column_id = all_columns.column_id
JOIN INFORMATION_SCHEMA.TABLES ist ON ist.TABLE_NAME = tables.name AND
                                      ist.TABLE_SCHEMA = schemas.name
WHERE all_columns.generated_always_type = 0 AND
      tables.name = '{}' AND
      schemas.name = '{}' AND
      all_columns.is_computed = 0"""

        local_tl = server_connect('localhost', 'SpreeTL')
        local_dw = server_connect('localhost', 'SpreeDW')

        if args.loaddimtables and not args.loadsynctables:
            full_load_script = load_spreedw_valuechain()
        elif args.loadsynctables and not args.loaddimtables:
            full_load_script = load_spreetl_valuechain(where_clause)
        else:
            full_load_script = load_spreetl_valuechain(where_clause) + '\n\n' + load_spreedw_valuechain()

        if args.verbose:
            print(full_load_script)

        if args.mode == 'copy':
            clipboard.copy(full_load_script)
        else:
            print('\n\nScript submitted')
            start_time = dt.datetime.now()
            statement(local_tl, full_load_script)
            end_time = dt.datetime.now()
            print('\n\nScript finished, runtime: ' + str(end_time - start_time))


