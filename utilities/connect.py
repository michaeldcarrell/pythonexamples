import pyodbc as odbc
import pandas as pd


def server_connect(sc_server, sc_database, sc_instance=""):
    if sc_instance != "":
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server}\\{sc_instance};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )
    else:
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )

    return connection


def query(connection, sql):
    return pd.read_sql(sql, connection)


def statement(connection, sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    cursor.commit()
