import pandas as pd
import pyodbc as odbc


def server_connect(server, database):
    connection = odbc.connect('Driver={SQL Server};'
                              f'Server={server}.lapkosoft.local;'
                              f'Database={database};'
                              'Trusted_Connection=yes')

    return connection


def get_pks(connection, database):
    sql = f"""
    SELECT '{database}' DatabaseName,
        OBJECT_NAME(ic.OBJECT_ID) AS TableName,
        CONCAT('{database}', '-', OBJECT_NAME(ic.OBJECT_ID)) TableKey,
        COL_NAME(ic.OBJECT_ID,ic.column_id) AS ColumnName
    FROM sys.indexes AS i
    INNER JOIN sys.index_columns AS ic
    ON i.OBJECT_ID = ic.OBJECT_ID
    AND i.index_id = ic.index_id
    WHERE i.is_primary_key = 1
    """

    return pd.read_sql(sql=sql, con=connection)


def get_spreetl_source_databases():
    connection = server_connect('azprod-bi-sql', 'SpreeTL_Dev')
    sql = """
SELECT DISTINCT
    SourceDatabase
FROM etl.SourceTable st
LEFT OUTER JOIN etl.PrimaryKeyCatalog pkc ON pkc.PrimaryKeyTableID = st.SourceTableID
WHERE pkc.PrimaryKeyID IS NULL
    """

    return pd.read_sql(sql=sql, con=connection)


def get_spreetl_source_tables():
    connection = server_connect('azprod-bi-sql', 'SpreeTL_Dev')
    sql = """
SELECT DISTINCT
    SourceTableName,
    SourceSchema,
    CONCAT(SourceDatabase, '-', SourceTableName) TableKey
FROM etl.SourceTable st
LEFT OUTER JOIN etl.PrimaryKeyCatalog pkc ON pkc.PrimaryKeyTableID = st.SourceTableID
WHERE pkc.PrimaryKeyID IS NULL
        """

    return pd.read_sql(sql=sql, con=connection)


def update_spreetl_key(currentkey):
    connection = server_connect('azprod-bi-sql', 'SpreeTL_Dev')
    cursor = connection.cursor()
    sql = f"""
EXEC etl.AddPrimaryKey
    @SourceTableName = '{currentkey['TableName']}',
    @SourceDatabaseName = '{currentkey['DatabaseName']}',
    @SourceSchemaName = '{currentkey['SourceSchema']}',
    @SourceTablePrimaryKey ='{currentkey['ColumnName']}',
    @ReplaceKey = 1
     """
    cursor.execute(sql)
    cursor.commit()


SpreeTLSourceDatabases = get_spreetl_source_databases()
SpreeTLSourceTables = get_spreetl_source_tables()


ServerKeyList = pd.DataFrame(columns=['DatabaseName', 'SourceSchema' 'TableName', 'TableKey', 'ColumnName'])

# Get All Primary Keys from Databases with SpreeTL_Dev.etl.SourceTable.SourceTablePrimaryKey
for index, database in SpreeTLSourceDatabases.iterrows():
    current_db_keys = get_pks(server_connect('zoidberg-ro', database['SourceDatabase']),
                              database=database['SourceDatabase'])
    ServerKeyList = ServerKeyList.append(current_db_keys)

SpreeTLNeededKeys = SpreeTLSourceTables.merge(ServerKeyList, how='left', on='TableKey')
SpreeTLNeededKeys = SpreeTLNeededKeys[SpreeTLNeededKeys['ColumnName'].notna()]

for index, key in SpreeTLNeededKeys.iterrows():
    print('Adding Key: ' + key['TableName'] + '.' + key['ColumnName'])
    update_spreetl_key(key)
    print('Key: ' + key['TableName'] + '.' + key['ColumnName'] + ' Added Successfully')
