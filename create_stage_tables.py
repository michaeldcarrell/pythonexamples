import pyodbc as odbc
import pandas as pd
import datetime as dt
import getpass
from colorama import init
import sys

sys.stdout.isatty()

init()  # Initializes the colors for text in console


def server_connect(sc_server, sc_database, sc_instance=""):
    if sc_instance != "":
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server}\\{sc_instance};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )
    else:
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )

    return connection


def query(connection, sql):
    return pd.read_sql(sql, connection)


def statement(connection, sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    cursor.commit()


def databases_from_server(server):
    databases = query(
        server_connect(server, "master"), "SELECT name Databases FROM sys.databases"
    )
    return databases


def schemas_from_database(server: str, sfd_database: str):
    schemas = query(
        server_connect(server, sfd_database), "SELECT name Schemas FROM sys.schemas"
    )
    return schemas


def tables_from_schema(server, tfs_database, tfs_schema):
    tfs_tables = query(
        server_connect(server, tfs_database),
        f"SELECT TABLE_NAME Tables FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{tfs_schema}'",
    )
    return tfs_tables


def create_spreetl_schema_if_needed(conn, source_database_name, source_schema_name):
    current_spreetl_schemas = query(conn, "SELECT LOWER(SCHEMA_NAME) SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA")['SCHEMA_NAME'].to_list()
    desired_schema_name = f'{source_database_name.lower()}_{source_schema_name.lower()}'
    if desired_schema_name not in current_spreetl_schemas:
        statement(conn, f"CREATE SCHEMA {desired_schema_name}")
    return desired_schema_name


def correct_formatting_for_pres(pres_string: str):
    return pres_string.replace('\n        ', '\n')


def column_structure_tables(table_name, column_structure, cst_stage_key, cst_etldata_key, cst_primary_key, dest_stage_schema_name):
    column_block = ""
    counter = 0
    for column in column_structure.iterrows():
        if counter != 0:
            newline = "\n"
        else:
            newline = ""
        column_block += (
            f"{newline}    ["
            + column[1]["Columns"]
            + "] "
            + column[1]["Data_Types"]
            + ", "
        )
        counter += 1

    row_hash_block = ""
    for column in column_structure.iterrows():
        row_hash_block += f"CONVERT(NVARCHAR, {column[1]['Columns']}) + '|' + "

    create_stage_table_sql = f"""
        CREATE TABLE {dest_stage_schema_name}.{table_name} (
            {cst_stage_key} INT IDENTITY(1,1),
        {column_block}
            IsIncrementalInsertOverlap BIT,
            IsSourceSystemActive BIT,
            SourceTableProcessExecutionID BIGINT,
            StageTableProcessExecutionID BIGINT,
            StatusTypeID INT NOT NULL DEFAULT 2,
            StateTypeID INT NOT NULL DEFAULT 0,
            RowHash VARBINARY(32),
            FirstSeenOn DATETIMEOFFSET,
            LastSeenOn DATETIMEOFFSET,
            ValidFrom datetime2(0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT {table_name}_ValidFrom DEFAULT SYSDATETIME(),
            ValidTill datetime2(0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT {table_name}_ValidTill DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
            CONSTRAINT [PK_{dest_stage_schema_name}_{table_name}_{cst_stage_key}] PRIMARY KEY CLUSTERED ([{cst_stage_key}] ASC),
            PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTill])
        );
        CREATE NONCLUSTERED INDEX IX_{dest_stage_schema_name}_{table_name}_{cst_primary_key} ON {dest_stage_schema_name}.{table_name}({cst_primary_key});
    """

    create_etldata_table_sql = f"""
        CREATE TABLE etldata.{table_name} (
            {cst_etldata_key} INT IDENTITY(1,1) NOT NULL,
        {column_block}
            ChangeOperation NCHAR(1),
            SourceTableProcessExecutionID BIGINT,
            StatusTypeID INT NOT NULL DEFAULT 2,
            StateTypeID INT NOT NULL DEFAULT 0,
            RowHash VARBINARY(32),
            CONSTRAINT [PK_etldata_{table_name}_{cst_etldata_key}] PRIMARY KEY CLUSTERED ([{cst_etldata_key}] ASC)
        );
        CREATE NONCLUSTERED INDEX IX_etldata_{table_name}_RowHash ON etldata.{table_name}(RowHash);
    """
    return {
        dest_stage_schema_name: {
            "sql": correct_formatting_for_pres(create_stage_table_sql),
            "table_name": f"{dest_stage_schema_name}.{table_name}"
        },
        "etldata": {
            "sql": correct_formatting_for_pres(create_etldata_table_sql),
            "table_name": f"etldata.{table_name}",
        },
    }


def check_table_exists_tl(ctet_table, ctet_schema, conn):
    stage_table_exists_sql = f"""
    SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_NAME = '{ctet_table}'
    AND TABLE_SCHEMA = '{ctet_schema}'
    """

    stage_table_exists = query(conn, stage_table_exists_sql)
    if len(stage_table_exists) == 1:
        return True
    else:
        return False


def column_structure_sp(
    schema_name,
    table_name,
    css_primary_key,
    css_stage_key,
    css_database,
    instance,
    column_structure,
):
    column_structure_sql = f"""
        SELECT COLUMN_NAME
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = '{table_name}'
            AND TABLE_SCHEMA = 'etldata'
            AND COLUMN_NAME NOT IN('ProcessTypeName', 'StatusTypeID', 'SourceTableProcessExecutionID', 'ChangeOperation', 'StateTypeID', 'RowHash')
            AND COLUMN_NAME NOT LIKE '%etldata%'"""

    try:
        primary_key_data_type = (
            column_structure[column_structure["PrimaryKeyFlag"] == 1]
            .reset_index()
            .at[0, "Data_Types"]
        )
    except KeyError:
        primary_key_data_type = "INT"

    column_structure = query(
        server_connect("localhost", css_database, instance), column_structure_sql
    )

    column_block = ""
    counter = 0
    for column in column_structure.iterrows():
        if column[1]["COLUMN_NAME"] not in [
            css_stage_key,
            "StatusTypeID",
            "StateTypeID",
            "ChangeOperation",
            "RowHash",
        ]:
            if counter != 0:
                newline = "\n"
            else:
                newline = ""
            column_block += f"{newline}\t\t[" + column[1]["COLUMN_NAME"] + "], "
            counter += 1

    column_block_etldata = ""
    counter = 0
    for column in column_structure.iterrows():
        if column[1]["COLUMN_NAME"] not in [
            css_stage_key,
            "StatusTypeID",
            "StateTypeID",
            "ChangeOperation",
            "Rowhash",
        ]:
            if counter != 0:
                newline = "\n"
            else:
                newline = ""
            column_block_etldata += (
                f"{newline}\t\tetldata.[" + column[1]["COLUMN_NAME"] + "], "
            )
            counter += 1

    column_block_update = ""
    counter = 0
    for column in column_structure.iterrows():
        if column[1]["COLUMN_NAME"] not in [
            "StateTypeID",
            "StatusTypeID",
            "ChangeOperation",
            css_stage_key,
        ]:
            if counter != 0:
                newline = "\n"
            else:
                newline = ""
            column_block_update += (
                f"{newline}\t\t["
                + column[1]["COLUMN_NAME"]
                + "] = etldata.["
                + column[1]["COLUMN_NAME"]
                + "], "
            )
            counter += 1

    sp_sql_status_state = f"""-- =====================================================================================================================
        -- Author: <DataInsights Team ({getpass.getuser()})>
        -- Creation Date: <{dt.date.today()}>
        -- Description: <This stored procedure will update status states for the {table_name} table>
        -- =====================================================================================================================

        CREATE PROCEDURE ssis.usp_etldata_{table_name}_Update_StatusState
            @SourceTableProcessExecutionID BIGINT,
            @StatusTypeID INT = 1,
            @StateTypeID INT = 1

        AS

        BEGIN
            SET NOCOUNT ON;

            UPDATE etldata.{table_name}
            SET
                StateTypeID = @StateTypeID,
                StatusTypeID = @StatusTypeID
            WHERE {table_name}.SourceTableProcessExecutionID = @SourceTableProcessExecutionID
            SET NOCOUNT OFF;
        END"""

    sp_sql_delete = f"""-- =====================================================================================================================
        -- Author: <DataInsights Team ({getpass.getuser()})>
        -- Creation Date: <{dt.date.today()}>
        -- Description: <This stored procedure will match deleted from source table {table_name} table>
        -- =====================================================================================================================

        CREATE PROCEDURE ssis.usp_etldata_{table_name}_Delete 
            @SourceTableProcessExecutionID BIGINT

        AS
        BEGIN
            SET NOCOUNT ON;
            DELETE FROM etldata.{table_name}
            WHERE StatusTypeID = 3
                AND StateTypeID = 3;
            SET NOCOUNT OFF;
        END;"""

    sp_sql_merge = f"""-- =====================================================================================================================
        -- Author: <DataInsights Team ({getpass.getuser()})>
        -- Creation Date: <{dt.date.today()}>
        -- Description: <This stored procedure will merge changes from source table {table_name}>
        -- =====================================================================================================================

        CREATE PROCEDURE [ssis].[usp_{schema_name}_Merge_{table_name}](
            @StageTableProcessExecutionID BIGINT,
            @VerboseFlag BIT = 1,
            @ProcessLimit BIGINT = 150000,
            @LockedRows INT OUTPUT,
            @NoChanges INT OUTPUT,
            @Updates INT OUTPUT,
            @SoftDeletes INT OUTPUT,
            @Inserts INT OUTPUT
        )
        AS
        BEGIN
            SET NOCOUNT ON;
            DECLARE @TRowsToUpdate TABLE({css_stage_key} INT NOT NULL PRIMARY KEY CLUSTERED, {css_primary_key} {primary_key_data_type}, RowHash varbinary(32), rn int);
            DECLARE @log AS [ssis].[ProcessExecutionLog];
            INSERT INTO @log(LogDescription)
            VALUES('starting [usp_{schema_name}_Merge_{table_name}]');
            INSERT INTO @log(LogDescription)
            VALUES('starting to lock records for processing');

            -- lock rows for update
            WITH RecordsToLock
                AS (SELECT TOP(@ProcessLimit)
                        etldata.{css_stage_key},
                        StateTypeID,
                        StatusTypeID,
                        etldata.{css_primary_key},
                        etldata.RowHash,
                        ROW_NUMBER() OVER(PARTITION BY etldata.{css_primary_key}
                        ORDER BY etldata.{css_stage_key}) AS rn
                    FROM etldata.{table_name} etldata WITH (ROWLOCK READPAST)
                    WHERE etldata.StatusTypeID = 1
                        AND etldata.StateTypeID = 1
                    ORDER BY etldata.{css_stage_key}
                )

            UPDATE RecordsToLock
            SET
                StateTypeID = 2
            OUTPUT inserted.{css_stage_key}, inserted.{css_primary_key}, inserted.RowHash, deleted.rn
                    INTO @TRowsToUpdate;

            SET @LockedRows = @@ROWCOUNT;

            INSERT INTO @log(LogDescription)
            VALUES('locked etldata rows for processing');

            -- I/U where LocationName is found an RowHash matches, then ust update LastSeenOn
            INSERT INTO @log(LogDescription)
            VALUES('updating no change records');

            UPDATE stage
            SET
                LastSeenOn = SYSDateTIMEOFFSET(),
                stage.SourceTableProcessExecutionID = etldata.SourceTableProcessExecutionID,
                stage.StageTableProcessExecutionID = @StageTableProcessExecutionID
            FROM {schema_name}.{table_name} stage
            INNER JOIN @TRowsToUpdate ttu ON stage.{css_primary_key} = ttu.{css_primary_key}
            INNER JOIN etldata.{table_name} etldata ON (ttu.{css_primary_key} = etldata.{css_primary_key}
                                                        AND stage.RowHash = etldata.RowHash)
            WHERE etldata.ChangeOperation IN ('I', 'U')
              AND ttu.rn = 1;

            SET @NoChanges = @@ROWCOUNT;
            INSERT INTO @log(LogDescription)
            VALUES('no change updates finished');

            --I/U where {css_primary_key} is found and RowHash does not match, then update
            INSERT INTO @log(LogDescription)
            VALUES('updating changed records');

            UPDATE stage
            SET
            {column_block_update}
                stage.LastSeenOn = SYSDATETIMEOFFSET(),
                stage.StatusTypeID = 1,
                stage.StateTypeID = 4,
                stage.RowHash = etldata.RowHash,
                stage.SourceTableProcessExecutionID = etldata.SourceTableProcessExecutionID,
                stage.StageTableProcessExecutionID = @StageTableProcessExecutionID
            FROM {schema_name}.{table_name} stage
            INNER JOIN @TRowsToUpdate ttu ON stage.{css_primary_key} = ttu.{css_primary_key}
            INNER JOIN etldata.{table_name} etldata ON (ttu.{css_primary_key} = etldata.{css_primary_key})
            WHERE etldata.ChangeOperation IN ('I', 'U')
              AND etldata.RowHash != ISNULL(stage.RowHash, CONVERT(varbinary(32), ''))
              AND ttu.rn = 1;

            SET @Updates = @@ROWCOUNT;
            INSERT INTO @log(LogDescription)
            VALUES('updates finished');

            -- D where {css_primary_key} matches, then set to inactivate
            INSERT INTO @log(LogDescription)
            VALUES('updating soft deletes');

            UPDATE stage
            SET
                stage.IsSourceSystemActive = 0,
                stage.StatusTypeID = 3,
                stage.StateTypeID = 4,
                stage.SourceTableProcessExecutionID = etldata.SourceTableProcessExecutionID,
                stage.StageTableProcessExecutionID = @StageTableProcessExecutionID
            FROM {schema_name}.{table_name} stage
            INNER JOIN @TRowsToUpdate ttu ON stage.{css_primary_key} = ttu.{css_primary_key}
            INNER JOIN etldata.{table_name} etldata ON (ttu.{css_primary_key} = etldata.{css_primary_key})
            WHERE etldata.ChangeOperation IN ('D')
              AND ttu.rn = 1;;

            SET @SoftDeletes = @@ROWCOUNT;
            INSERT INTO @log(LogDescription)
            VALUES('soft deletes finished');

            -- Insert records that do not exist
            INSERT INTO @log(LogDescription)
            VALUES('processing new records');

            INSERT INTO {schema_name}.{table_name} (
            {column_block}
                IsIncrementalInsertOverlap,
                IsSourceSystemActive,
                StatusTypeID,
                StateTypeID,
                FirstSeenOn,
                LastSeenOn,
                StageTableProcessExecutionID,
                RowHash,
                SourceTableProcessExecutionID
            )
            SELECT
            {column_block_etldata}
                0 IsIncrementalInsertOverlap,
                1 IsSourceSystemActive,
                1 StatusTypeID, --Active
                1 StateTypeID, --Is Available
                SYSDATETIMEOFFSET() FirstSeenOn,
                SYSDATETIMEOFFSET() LastSeenOn,
                @StageTableProcessExecutionID,
                etldata.RowHash,
                etldata.SourceTableProcessExecutionID
            FROM etldata.{table_name} etldata
            INNER JOIN @TRowsToUpdate ttu ON etldata.{css_stage_key} = ttu.{css_stage_key}
            LEFT OUTER JOIN {schema_name}.{table_name} stage ON etldata.RowHash = stage.RowHash
            WHERE etldata.ChangeOperation IN ('I', 'U')
            AND stage.RowHash IS NULL
            AND ttu.rn = 1
            AND NOT EXISTS (
                SELECT 1
                FROM {schema_name}.{table_name} t2
                WHERE t2.{css_primary_key} = etldata.{css_primary_key}
            );

            SET @Inserts = @@ROWCOUNT;
            INSERT INTO @log(LogDescription)
            VALUES('[usp_{schema_name}_Merge_{table_name}] finished');

            IF @VerboseFlag = 1
                EXEC [ssis].usp_etl_Load_StageTableProcessExecution_Log 
                    @StageTableProcessExecutionID = @StageTableProcessExecutionID,
                    @LogTable = @log;

            --Unlock Records
            UPDATE etldata
            -- StatusTypeID/StateTypeID = (3, 3) means it's available for the etldata process to delete
            SET StatusTypeID = 3,
                StateTypeID = 3
            FROM etldata.{table_name} etldata
            JOIN @TRowsToUpdate ttu ON etldata.{css_stage_key} = ttu.{css_stage_key};

            SET NOCOUNT OFF;
        END;"""
    return {
        "status_state": {
            "sql": correct_formatting_for_pres(sp_sql_status_state),
            "proc_name": f"usp_etldata_{table_name}_Update_StatusState",
        },
        "delete": {
            "sql": correct_formatting_for_pres(sp_sql_delete),
            "proc_name": f"usp_etldata_{table_name}_Delete ",
        },
        "merge": {
            "sql": correct_formatting_for_pres(sp_sql_merge),
            "proc_name": f"usp_{schema_name}_Merge_{table_name}",
        },
    }


def get_row_hash(table_name, conn, row_hash_stage_key):
    get_hash_query = f"""WITH cte_hash_stmt
     AS (SELECT c.TABLE_SCHEMA AS SchemaName,
                c.TABLE_NAME AS TableName,
                c.COLUMN_NAME AS ColumnName,
                c.DATA_TYPE AS DataType,
                (CASE
                    WHEN c.DATA_TYPE IN ('varchar', 'nvarchar')
                     THEN 'ISNULL(CONVERT( nvarchar(' + CAST(character_maximum_length AS VARCHAR) +'), source.[' + c.COLUMN_NAME + ']), '''')'
                     WHEN c.DATA_TYPE = 'datetime'
                     THEN 'ISNULL(CONVERT(nvarchar, source.[' + c.COLUMN_NAME + '], 127), '''')'
                     WHEN c.DATA_TYPE IN('decimal')
                     THEN 'ISNULL(CONVERT(nvarchar, source.[' + c.COLUMN_NAME + ']), 0)'
                     ELSE 'ISNULL(CONVERT(nvarchar, source.[' + c.COLUMN_NAME + ']), '''')'
                 END) ConvertStmt
         FROM information_schema.columns AS c
         WHERE c.TABLE_SCHEMA IN('etldata')
         AND c.COLUMN_NAME NOT IN('ChangeOperation', 'SourceTableProcessExecutionID', 'StateTypeID', 'StatusTypeID', 'RowHash', '{row_hash_stage_key}')
     AND c.COLUMN_NAME NOT LIKE '%etldata%')
     SELECT chs.SchemaName,
            chs.TableName,
            'CONVERT(varbinary(32), HASHBYTES(''SHA2_256'', ' + STRING_AGG(chs.ConvertStmt, ' + ''|'' + ') + ')) as RowHash' AS HashStmt
     FROM cte_hash_stmt chs
     WHERE chs.SchemaName = 'etldata'
           AND chs.TableName = '{table_name}'
     GROUP BY chs.SchemaName,
              chs.TableName;"""
    return query(conn, get_hash_query)['HashStmt'][0]


def column_structure_ssis(column_structure, schema_name, table_name, css_primary_key, css_stage_key, ssis_row_hash):
    column_block = ""
    column_block_source = ""
    counter = 0
    pk_column_structure = {
        "column_name": "",
        "data_type": ""
    }
    for column in column_structure.iterrows():
        if column[1]["Columns"] not in [css_primary_key, css_stage_key]:
            if counter != 0:
                newline = "\n"
            else:
                newline = ""
            column_block += f"{newline}    CAST([{column[1]['Columns']}] AS {column[1]['Data_Types']}) [{column[1]['Columns']}], "
            column_block_source += f"{newline}    CAST(source.[{column[1]['Columns']}] AS {column[1]['Data_Types']}) [{column[1]['Columns']}], "
            counter += 1
        elif column[1]["Columns"] in [css_primary_key]:
            pk_column_structure = {
                "column_name": column[1]["Columns"],
                "data_type": column[1]["Data_Types"]
            }
    ssis_statement = f"""( @[$Package::ProcessTypeName]  =="Full Load")?
        "
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        SELECT "+(@[$Package::TestingMode]?" TOP 100 ":"")+"
            CAST({pk_column_structure['column_name']} AS {pk_column_structure['data_type']}) [{pk_column_structure['column_name']}],--Primary Key
            /***BEGIN Non-PK fields for this table***/
        {column_block}
            /***END Non-PK fields for this table***/	
            CAST('I' AS NCHAR(1)) AS [ChangeOperation],
            {ssis_row_hash}
        FROM {schema_name}.{table_name} source
        WHERE 1=1
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        ":"
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        SELECT
            CAST(source_changetracking.{pk_column_structure['column_name']} AS {pk_column_structure['data_type']}) [{pk_column_structure['column_name']}], --Primary Key
            /***BEGIN Non-PK fields for this table***/
        {column_block_source}
            /***END Non-PK fields for this table***/
            CAST(source_changetracking.SYS_CHANGE_OPERATION AS NCHAR(1)) AS ChangeOperation,
            {ssis_row_hash}
        FROM ChangeTable(CHANGES {schema_name}.{table_name}, "+(DT_WSTR,20)@[template::ChangeTrackingCurrentVersionForCurrentLoad]+") source_changetracking
        INNER JOIN {schema_name}.{table_name} source ON source_changetracking.{css_primary_key} = source.{css_primary_key}
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        "
    """
    return correct_formatting_for_pres(ssis_statement)


def get_column_data_types(cdt_conns, cdt_table, cdt_schema):
    column_data_type_sql = f"""
        WITH PrimaryKeyFlags
            AS (SELECT tc.TABLE_CATALOG, 
                        tc.TABLE_SCHEMA, 
                        tc.TABLE_NAME, 
                        ccu.COLUMN_NAME, 
                        ccu.CONSTRAINT_NAME, 
                        1 AS PRIMARY_KEY_FLAG
                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                    JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_SCHEMA = ccu.TABLE_SCHEMA
                                                                            AND tc.TABLE_NAME = ccu.TABLE_NAME
                                                                            AND tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
                WHERE tc.CONSTRAINT_TYPE = 'PRIMARY KEY')
        SELECT c.COLUMN_NAME Columns, 
                UPPER(CASE
                        WHEN c.DATA_TYPE LIKE 'text'
                        THEN 'text'
                        WHEN c.CHARACTER_MAXIMUM_LENGTH IS NOT NULL
                        THEN CONCAT(c.DATA_TYPE, '(', REPLACE(c.CHARACTER_MAXIMUM_LENGTH, '-1', 'MAX'), ')')
                        ELSE c.DATA_TYPE
                    END) Data_Types,
                ISNULL(pkf.PRIMARY_KEY_FLAG, 0) AS PrimaryKeyFlag
        FROM INFORMATION_SCHEMA.COLUMNS c
            LEFT JOIN PrimaryKeyFlags pkf ON c.COLUMN_NAME = pkf.COLUMN_NAME
                                            AND c.TABLE_CATALOG = pkf.TABLE_CATALOG
                                            AND c.TABLE_SCHEMA = pkf.TABLE_SCHEMA
                                            AND c.TABLE_NAME = pkf.TABLE_NAME
        WHERE c.TABLE_NAME = '{cdt_table}'
          AND c.TABLE_SCHEMA = '{cdt_schema}'
        """
    return query(cdt_conns["source"], column_data_type_sql)


def print_request_input(string):
    sys.stdout.write("\x1b[1;33m" + string + "\x1b[0m" + "\n")
    sys.stdout.flush()
    return input()


def print_error_input(string):
    sys.stdout.write("\x1b[1;31m" + string + "\x1b[0m" + "\n")
    sys.stdout.flush()
    return input()


def print_error(string):
    sys.stdout.write("\x1b[1;31m" + string + "\x1b[0m" + "\n")


def print_success_input(string):
    sys.stdout.write("\x1b[1;32m" + string + "\x1b[0m" + "\n")
    sys.stdout.flush()
    return input()


def print_success(string):
    sys.stdout.write("\x1b[1;32m" + string + "\x1b[0m" + "\n")


def choose_dest_database():
    dest = print_request_input("Local SpreeTL database name (default 'SpreeTL'): ")
    if dest == "":
        dest = "SpreeTL"
    return dest


def choose_dest_instance():
    dest = print_request_input("Local instance name (default [blank]): ")
    return dest


def choose_source_server():
    src = print_request_input(
        'Specify source server to collect table from (default "zoidberg"): '
    )
    if src == "":
        src = "zoidberg-ro"
    return src


def choose_source_database(source_server_inpt):
    dbs = databases_from_server(source_server_inpt)
    print("\n")
    print(dbs)
    database_index = print_request_input("Specify Database (default 0): ")
    if database_index == "":
        db = dbs["Databases"][0]
    else:
        db = dbs["Databases"][int(database_index)]
    return db


def choose_source_schema(css_source_server, css_database):
    schemas = schemas_from_database(css_source_server, css_database)
    print("\n")
    print(schemas)
    schema_index = print_request_input("Specify Schema (default 0): ")
    if schema_index == "":
        css_schema = schemas["Schemas"][0]
    else:
        css_schema = schemas["Schemas"][int(schema_index)]
    return css_schema


def produce_connections(
    pc_source_server: str,
    pc_source_database: str,
    pc_dest_server: str,
    pc_dest_instance: str,
    pc_dest_database: str,
):
    source_conn = server_connect(pc_source_server, pc_source_database)
    dest_conn = server_connect(pc_dest_server, pc_dest_database, pc_dest_instance)

    return {"source": source_conn, "dest": dest_conn}


if __name__ == "__main__":

    pd.set_option("display.max_rows", None)
    pd.set_option("display.max_columns", None)

    next_table = True

    dest_spreetl = choose_dest_database()
    dest_instance = choose_dest_instance()
    source_server = choose_source_server()
    source_database = choose_source_database(source_server)
    schema = choose_source_schema(source_server, source_database)

    while next_table:

        conns = produce_connections(
            source_server, source_database, "localhost", dest_instance, dest_spreetl
        )

        tables = tables_from_schema(source_server, source_database, schema)
        print("\n")
        print(tables)

        table_index = print_request_input("Specify Table (default 0): ")
        if table_index == "":
            table = tables["Tables"][0]
        else:
            table = tables["Tables"][int(table_index)]

        table_address = source_database + "." + schema + "." + table

        column_data_types = get_column_data_types(conns, table, schema)
        looper = True

        while looper:
            print("\nCurrent Columns:")
            print(column_data_types["Columns"])
            removal_columns = print_request_input(
                "Specify Columns for Removal (Separate Multiples with ',') To Continue([Enter]): "
            )
            if removal_columns != "":
                removal_list = list(removal_columns.replace(" ", "").replace(",", ""))
                for i in range(0, len(removal_list)):
                    removal_list[i] = int(removal_list[i])
                column_data_types = column_data_types.drop(
                    column_data_types.index[removal_list]
                )
                column_data_types = column_data_types.reset_index(drop=True)
            else:
                looper = False

        looper = True

        while looper:
            print("\nCurrent Data Types: ")
            print(column_data_types)
            column_data_type_to_change = print_request_input(
                "Specify Data Type to Change. To Continue([Enter]): "
            )
            if column_data_type_to_change != "":
                data_type_change_to = print_request_input(
                    f"Specify New Data Type for {column_data_types['Columns'][int(column_data_type_to_change)]}:"
                )
                column_data_types["Data_Types"][
                    int(column_data_type_to_change)
                ] = data_type_change_to.upper()
            else:
                looper = False

        print("\n")
        print(column_data_types)
        primary_key_input = print_request_input(
            f"Select Primary Key (default {column_data_types['Columns'][0]}): "
        )
        if primary_key_input == "":
            primary_key = column_data_types["Columns"][0]
        else:
            primary_key = column_data_types["Columns"][int(primary_key_input)]

        stage_key = "RecordKey"

        dest_schema_name = create_spreetl_schema_if_needed(conns['dest'], source_database, schema)

        tables_sql = column_structure_tables(table, column_data_types, stage_key, dest_schema_name)
        for dest_schema in tables_sql:
            print(tables_sql[dest_schema]["sql"])
            tables_submit = print_request_input("Submit above table (Y/n)?: ")
            drop = "n"
            if tables_submit.lower() == "y" or tables_submit == "":
                table_exists = False
                if check_table_exists_tl(table, dest_schema, conns["dest"]):
                    table_exists = True
                    if dest_schema in ["stage", "history"]:
                        drop_text = f"""{tables_sql[dest_schema]['table_name']} already exists.
Since this is a temporal table both stage and history schema tables must be dropped.
Drop and re-create BOTH history.Stage{table}History AND stage.{table} tables? (y/N): 
"""
                    else:
                        drop_text = f"{tables_sql[dest_schema]['table_name']} already exists.\nDrop and re-create table? (y/N): "
                    drop = print_error_input(drop_text)
                    if drop == "y" and dest_schema not in ["stage"]:
                        tables_sql[dest_schema]["sql"] = (
                            f"DROP TABLE IF EXISTS {tables_sql[dest_schema]['table_name']};"
                            + tables_sql[dest_schema]["sql"]
                        )
                    elif drop == "y" and dest_schema in ["stage"]:
                        tables_sql[dest_schema]["sql"] = (
                            f"""
ALTER TABLE stage.{table} SET (SYSTEM_VERSIONING = OFF);
DROP TABLE IF EXISTS history.Stage{table}History;
DROP TABLE IF EXISTS stage.{table};
"""
                            + tables_sql[dest_schema]["sql"]
                        )
                if drop == "y" or not table_exists:
                    print(tables_sql[dest_schema]["sql"])
                    statement(conns["dest"], tables_sql[dest_schema]["sql"])
                    print_success("Tables Submitted Successfully!")

        sp_code = column_structure_sp(
            dest_schema_name,
            table,
            primary_key,
            stage_key,
            dest_spreetl,
            dest_instance,
            column_data_types
        )

        for sp in sp_code:
            proc_exists_sql = f"""SELECT SPECIFIC_NAME
        FROM INFORMATION_SCHEMA.ROUTINES
        WHERE SPECIFIC_SCHEMA = 'ssis'
        AND SPECIFIC_NAME = '{sp_code[sp]['proc_name']}'
        """
            print(sp_code[sp]["sql"])
            sp_submit = print_request_input("Submit Above Store Procedure? (Y/n): ")
            if sp_submit.lower() == "y" or sp_submit == "":
                sp_exists = len(query(conns["dest"], proc_exists_sql)) >= 1
                if sp_exists:
                    drop = print_error_input(
                        "Procedure already exists. Do you want to overwrite it? (y/N): "
                    )
                    if drop.lower() == "y":
                        statement(
                            conns["dest"],
                            sp_code[sp]["sql"].replace(
                                "CREATE PROCEDURE", "CREATE OR ALTER PROCEDURE"
                            ),
                        )
                        print_success("Procedure Successfully Submitted!")
                else:
                    statement(conns["dest"], sp_code[sp]["sql"])
                    print_success("Procedure Successfully Submitted!")

        print("\n\n")
        print(column_structure_ssis(column_data_types, schema, table, primary_key, stage_key, get_row_hash(table, conns['dest'], stage_key)))
        print("\n\n")

        changes = True
        while changes:
            to_change = print_request_input(
                "To change source server (ss), database (db) or schema (s), else [Enter] to continue with next table: "
            )
            if to_change == "":
                break
            elif to_change == "ss":
                source_server = choose_source_server()
                source_database = choose_source_database(source_server)
                schema = choose_source_schema(source_server, source_database)
            elif to_change == "db":
                source_database = choose_source_database(source_server)
                schema = choose_source_schema(source_server, source_database)
            elif to_change == "s":
                schema = choose_source_schema(source_server, source_database)
            elif to_change == "":
                changes = False
            else:
                print_error(
                    "Unexpected input. To change source server (ss), database (db)"
                    " or schema (s), else [Enter] to continue with next table: "
                )
