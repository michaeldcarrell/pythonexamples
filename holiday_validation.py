from datetime import date, timedelta
import holidays
import pyodbc as odbc
import pandas as pd


def server_connect(sc_server, sc_database, sc_instance=""):
    if sc_instance != "":
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server}\\{sc_instance};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )
    else:
        connection = odbc.connect(
            "Driver={ODBC Driver 17 for SQL Server};"
            f"Server={sc_server};"
            f"Database={sc_database};"
            "Trusted_Connection=yes"
        )

    return connection


def query(connection, sql):
    return pd.read_sql(sql, connection)


def statement(connection, sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    cursor.commit()


us_holidays = holidays.US()


start_year = 2005
stop_year = 2030
years = [year for year in range(start_year, stop_year + 1)]
major_holidays_view = holidays.US(years=years).items()
major_holidays = holidays.US(years=years)

for date, name in major_holidays_view:
    if name == 'Thanksgiving':
        major_holidays.append({date + timedelta(days=1): "Black Friday"})
        major_holidays.append({date + timedelta(days=4): "Cyber Monday"})

update_query = """DECLARE
    @STARTDATE DATE = (SELECT
                           MIN(Date)
                       FROM dim.Date),
    @STOPDATE  DATE = (SELECT
                           MAX(Date)
                       FROM dim.Date);

WITH majorHolidays AS (
    SELECT
        Date,
        CASE
            WHEN month = 1
                AND day = 1
                THEN 'New Years Day'
            WHEN DATEPART(DAY, Date) BETWEEN 15 AND 21
                AND DATEPART(MONTH, Date) = 1
                AND DATEPART(WEEKDAY, Date) = 2
                AND DATEPART(YEAR, Date) >= 1996
                THEN 'Birthday of Martin Luther King, Jr.'
            WHEN DATEPART(DAY, Date) BETWEEN 15 AND 21
                AND DATEPART(MONTH, Date) = 2
                AND DATEPART(WEEKDAY, Date) = 2
                THEN 'Washington''s Birthday'
            WHEN CE.CATHOLIC_EASTER_SUNDAY IS NOT NULL
                THEN 'Catholic Easter Sunday'
            WHEN OE.ORTHODOX_EASTER_SUNDAY IS NOT NULL
                THEN 'Orthodox Easter Sunday'
            WHEN DATEPART(DAY, Date) BETWEEN 25 AND 31
                AND DATEPART(MONTH, Date) = 5
                AND DATEPART(WEEKDAY, Date) = 2
                THEN 'Memorial Day'
            WHEN DATEPART(DAY, Date) = 4
                AND DATEPART(MONTH, Date) = 7
                THEN 'Independence Day'
            WHEN DATEPART(DAY, Date) BETWEEN 1 AND 7
                AND DATEPART(MONTH, Date) = 9
                AND DATEPART(WEEKDAY, Date) = 2
                THEN 'Labor Day'
            WHEN DATEPART(DAY, Date) BETWEEN 8 AND 14
                AND DATEPART(MONTH, Date) = 10
                AND DATEPART(WEEKDAY, Date) = 2
                THEN 'Columbus Day'
            WHEN DATEPART(DAY, Date) = 11
                AND DATEPART(MONTH, Date) = 11
                THEN 'Veterans Day'
            WHEN DATEPART(DAY, Date) BETWEEN 22 AND 28
                AND DATEPART(MONTH, Date) = 11
                AND DATEPART(WEEKDAY, Date) = 5
                THEN 'Thanksgiving Day'
            WHEN DATEPART(DAY, Date) BETWEEN 23 AND 29
                AND DATEPART(MONTH, Date) = 11
                AND DATEPART(WEEKDAY, Date) = 6
                THEN 'Black Friday'
            WHEN DATEPART(DAY, DATEADD(DAY, -3, Date)) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DATEADD(DAY, -3, Date)) = 11
                AND DATEPART(WEEKDAY, DATEADD(DAY, -3, Date)) = 6
                THEN 'Cyber Monday'
            WHEN DATEPART(DAY, Date) = 25
                AND DATEPART(MONTH, Date) = 12
                THEN 'Christmas Day'
            ELSE 'Not a Holiday'
            END MajorHolidays/*,
        CASE
            WHEN MonthPreviousYear = 1
                AND DayPreviousYear = 1
                THEN 'New Years Day'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousYear) = 1
                AND DATEPART(WEEKDAY, DatePreviousYear) = 2
                AND DATEPART(YEAR, DatePreviousYear) >= 1996
                THEN 'Birthday of Martin Luther King, Jr.'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousYear) = 2
                AND DATEPART(WEEKDAY, DatePreviousYear) = 2
                THEN 'Washington''s Birthday'
            WHEN PYCE.CATHOLIC_EASTER_SUNDAY IS NOT NULL
                THEN 'Catholic Easter Sunday'
            WHEN PYOE.ORTHODOX_EASTER_SUNDAY IS NOT NULL
                THEN 'Orthodox Easter Sunday'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 25 AND 31
                AND DATEPART(MONTH, DatePreviousYear) = 5
                AND DATEPART(WEEKDAY, DatePreviousYear) = 2
                THEN 'Memorial Day'
            WHEN DATEPART(DAY, DatePreviousYear) = 4
                AND DATEPART(MONTH, DatePreviousYear) = 7
                THEN 'Independence Day'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 1 AND 7
                AND DATEPART(MONTH, DatePreviousYear) = 9
                AND DATEPART(WEEKDAY, DatePreviousYear) = 2
                THEN 'Labor Day'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 8 AND 14
                AND DATEPART(MONTH, DatePreviousYear) = 10
                AND DATEPART(WEEKDAY, DatePreviousYear) = 2
                THEN 'Columbus Day'
            WHEN DATEPART(DAY, DatePreviousYear) = 11
                AND DATEPART(MONTH, DatePreviousYear) = 11
                THEN 'Veterans Day'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 22 AND 28
                AND DATEPART(MONTH, DatePreviousYear) = 11
                AND DATEPART(WEEKDAY, DatePreviousYear) = 5
                THEN 'Thanksgiving Day'
            WHEN DATEPART(DAY, DatePreviousYear) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DatePreviousYear) = 11
                AND DATEPART(WEEKDAY, DatePreviousYear) = 6
                THEN 'Black Friday'
            WHEN DATEPART(DAY, DATEADD(DAY, -3, DatePreviousYear)) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DATEADD(DAY, -3, DatePreviousYear)) = 11
                AND DATEPART(WEEKDAY, DATEADD(DAY, -3, DatePreviousYear)) = 6
                THEN 'Cyber Monday'
            WHEN DATEPART(DAY, DatePreviousYear) = 25
                AND DATEPART(MONTH, DatePreviousYear) = 12
                THEN 'Christmas Day'
            ELSE 'Not a Holiday'
            END MajorHolidayPreviousYear,
        CASE
            WHEN MonthPreviousQuarter = 1
                AND DayPreviousQuarter = 1
                THEN 'New Years Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousQuarter) = 1
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 2
                AND DATEPART(YEAR, DatePreviousQuarter) >= 1996
                THEN 'Birthday of Martin Luther King, Jr.'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousQuarter) = 2
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 2
                THEN 'Washington''s Birthday'
            WHEN PQCE.CATHOLIC_EASTER_SUNDAY IS NOT NULL
                THEN 'Catholic Easter Sunday'
            WHEN PQOE.ORTHODOX_EASTER_SUNDAY IS NOT NULL
                THEN 'Orthodox Easter Sunday'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 25 AND 31
                AND DATEPART(MONTH, DatePreviousQuarter) = 5
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 2
                THEN 'Memorial Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) = 4
                AND DATEPART(MONTH, DatePreviousQuarter) = 7
                THEN 'Independence Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 1 AND 7
                AND DATEPART(MONTH, DatePreviousQuarter) = 9
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 2
                THEN 'Labor Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 8 AND 14
                AND DATEPART(MONTH, DatePreviousQuarter) = 10
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 2
                THEN 'Columbus Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) = 11
                AND DATEPART(MONTH, DatePreviousQuarter) = 11
                THEN 'Veterans Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 22 AND 28
                AND DATEPART(MONTH, DatePreviousQuarter) = 11
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 5
                THEN 'Thanksgiving Day'
            WHEN DATEPART(DAY, DatePreviousQuarter) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DatePreviousQuarter) = 11
                AND DATEPART(WEEKDAY, DatePreviousQuarter) = 6
                THEN 'Black Friday'
            WHEN DATEPART(DAY, DATEADD(DAY, -3, DatePreviousQuarter)) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DATEADD(DAY, -3, DatePreviousQuarter)) = 11
                AND DATEPART(WEEKDAY, DATEADD(DAY, -3, DatePreviousQuarter)) = 6
                THEN 'Cyber Monday'
            WHEN DATEPART(DAY, DatePreviousQuarter) = 25
                AND DATEPART(MONTH, DatePreviousQuarter) = 12
                THEN 'Christmas Day'
            ELSE 'Not a Holiday'
            END MajorHolidayPreviousQuarter,
        CASE
            WHEN MonthPreviousMonth = 1
                AND DayPreviousMonth = 1
                THEN 'New Years Day'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousMonth) = 1
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 2
                AND DATEPART(YEAR, DatePreviousMonth) >= 1996
                THEN 'Birthday of Martin Luther King, Jr.'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousMonth) = 2
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 2
                THEN 'Washington''s Birthday'
            WHEN PMCE.CATHOLIC_EASTER_SUNDAY IS NOT NULL
                THEN 'Catholic Easter Sunday'
            WHEN PMOE.ORTHODOX_EASTER_SUNDAY IS NOT NULL
                THEN 'Orthodox Easter Sunday'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 25 AND 31
                AND DATEPART(MONTH, DatePreviousMonth) = 5
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 2
                THEN 'Memorial Day'
            WHEN DATEPART(DAY, DatePreviousMonth) = 4
                AND DATEPART(MONTH, DatePreviousMonth) = 7
                THEN 'Independence Day'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 1 AND 7
                AND DATEPART(MONTH, DatePreviousMonth) = 9
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 2
                THEN 'Labor Day'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 8 AND 14
                AND DATEPART(MONTH, DatePreviousMonth) = 10
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 2
                THEN 'Columbus Day'
            WHEN DATEPART(DAY, DatePreviousMonth) = 11
                AND DATEPART(MONTH, DatePreviousMonth) = 11
                THEN 'Veterans Day'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 22 AND 28
                AND DATEPART(MONTH, DatePreviousMonth) = 11
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 5
                THEN 'Thanksgiving Day'
            WHEN DATEPART(DAY, DatePreviousMonth) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DatePreviousMonth) = 11
                AND DATEPART(WEEKDAY, DatePreviousMonth) = 6
                THEN 'Black Friday'
            WHEN DATEPART(DAY, DATEADD(DAY, -3, DatePreviousMonth)) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DATEADD(DAY, -3, DatePreviousMonth)) = 11
                AND DATEPART(WEEKDAY, DATEADD(DAY, -3, DatePreviousMonth)) = 6
                THEN 'Cyber Monday'
            WHEN DATEPART(DAY, DatePreviousMonth) = 25
                AND DATEPART(MONTH, DatePreviousMonth) = 12
                THEN 'Christmas Day'
            ELSE 'Not a Holiday'
            END MajorHolidayPreviousMonth,
        CASE
            WHEN MonthPreviousWeek = 1
                AND DayPreviousWeek = 1
                THEN 'New Years Day'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousWeek) = 1
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 2
                AND DATEPART(YEAR, DatePreviousWeek) >= 1996
                THEN 'Birthday of Martin Luther King, Jr.'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 15 AND 21
                AND DATEPART(MONTH, DatePreviousWeek) = 2
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 2
                THEN 'Washington''s Birthday'
            WHEN PWCE.CATHOLIC_EASTER_SUNDAY IS NOT NULL
                THEN 'Catholic Easter Sunday'
            WHEN PWOE.ORTHODOX_EASTER_SUNDAY IS NOT NULL
                THEN 'Orthodox Easter Sunday'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 25 AND 31
                AND DATEPART(MONTH, DatePreviousWeek) = 5
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 2
                THEN 'Memorial Day'
            WHEN DATEPART(DAY, DatePreviousWeek) = 4
                AND DATEPART(MONTH, DatePreviousWeek) = 7
                THEN 'Independence Day'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 1 AND 7
                AND DATEPART(MONTH, DatePreviousWeek) = 9
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 2
                THEN 'Labor Day'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 8 AND 14
                AND DATEPART(MONTH, DatePreviousWeek) = 10
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 2
                THEN 'Columbus Day'
            WHEN DATEPART(DAY, DatePreviousWeek) = 11
                AND DATEPART(MONTH, DatePreviousWeek) = 11
                THEN 'Veterans Day'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 22 AND 28
                AND DATEPART(MONTH, DatePreviousWeek) = 11
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 5
                THEN 'Thanksgiving Day'
            WHEN DATEPART(DAY, DatePreviousWeek) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DatePreviousWeek) = 11
                AND DATEPART(WEEKDAY, DatePreviousWeek) = 6
                THEN 'Black Friday'
            WHEN DATEPART(DAY, DATEADD(DAY, -3, DatePreviousWeek)) BETWEEN 23 AND 29
                AND DATEPART(MONTH, DATEADD(DAY, -3, DatePreviousWeek)) = 11
                AND DATEPART(WEEKDAY, DATEADD(DAY, -3, DatePreviousWeek)) = 6
                THEN 'Cyber Monday'
            WHEN DATEPART(DAY, DatePreviousWeek) = 25
                AND DATEPART(MONTH, DatePreviousWeek) = 12
                THEN 'Christmas Day'
            ELSE 'Not a Holiday'
            END MajorHolidayPreviousWeek*/
    FROM dim.Date d
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) oe ON oe.Orthodox_Easter_Sunday = d.Date
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pwoe ON pwoe.Orthodox_Easter_Sunday = d.DatePreviousWeek
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pmoe ON pmoe.Orthodox_Easter_Sunday = d.DatePreviousMonth
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pqoe ON pqoe.Orthodox_Easter_Sunday = d.DatePreviousQuarter
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pyoe ON pyoe.Orthodox_Easter_Sunday = d.DatePreviousYear
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) ce ON ce.Catholic_Easter_Sunday = d.Date
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pwce ON pwce.Catholic_Easter_Sunday = d.DatePreviousWeek
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pmce ON pmce.Catholic_Easter_Sunday = d.DatePreviousMonth
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pqce ON pqce.Catholic_Easter_Sunday = d.DatePreviousQuarter
    LEFT OUTER JOIN dim.Easter(@STARTDATE, @STOPDATE) pyce ON pyce.Catholic_Easter_Sunday = d.DatePreviousYear
)
SELECT
    Date,
    MajorHolidays
FROM majorHolidays
WHERE MajorHolidays != 'Not a Holiday'"""

spreedw = server_connect('localhost', 'SpreeDW')
dw_holidays = query(spreedw, update_query)

for index, holiday in dw_holidays.iterrows():
    current_holiday = major_holidays.get(holiday['Date'])
    if not current_holiday:
        print_statement = f"Date: {holiday['Date']}, Holiday: {current_holiday} - DW: {holiday['MajorHolidays']}\n"
        print(print_statement)

