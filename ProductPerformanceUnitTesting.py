import utilities.connect as dbc
import pandas as pd
import sqlalchemy as sql
import warnings
import numpy as np

local_dw_engine = sql.create_engine("mssql+pyodbc://localhost/SpreeDW?trusted_connection=yes&driver=SQL+Server+Native+Client+11.0")
local_validation_engine = sql.create_engine("mssql+pyodbc://localhost/Validation?trusted_connection=yes&driver=SQL+Server+Native+Client+11.0")

int_dw = sql.create_engine("mssql+pyodbc://azint-bi-sql/SpreeTL?trusted_connection=yes&driver=SQL+Server+Native+Client+11.0")


