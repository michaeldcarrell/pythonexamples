import utilities.connect as dbc
import pandas as pd

pd.options.display.max_rows = 999
pd.options.display.max_columns = 999

local_tl = dbc.server_connect('localhost', 'SpreeTL')
local_dw = dbc.server_connect('localhost', 'SpreeDW')
local_validation = dbc.server_connect('localhost', 'Validation')

vc_referenced_procs_tl = [
    'ssis.usp_stage_Merge_GeneratedRMAItems',
    '[ssis].[usp_stage_Merge_FlatSalesValues]',
    'ssis.usp_stage_Merge_DetailedCOGS',
    '[ssis].[usp_temp_ValueChain_PrepLoad]',
    'ssis.usp_temp_Load_PackageItems',
    'ssis.usp_temp_Load_SaleRefund',
    'ssis.usp_temp_Load_SaleNegativeProfit',
    'ssis.usp_temp_Load_SaleItemRefund',
    'ssis.usp_temp_Load_OrderDate',
    'ssis.usp_temp_Load_SaleItemReturn',
    'ssis.usp_temp_Load_PackageCleaned',
    'ssis.usp_temp_ValueChainInitialPull',
    'temp.usp_ValueChain_Load_Sale',
    'temp.usp_ValueChain_Load_Rebates',
    'temp.usp_ValueChain_Load_Counts',
    'temp.usp_ValueChain_Load_Promotion',
    'ssis.usp_temp_cc',
    'temp.usp_ValueChain_Load_Aggregation',
    'temp.usp_ValueChain_Load_Attribution',
    'ssis.usp_temp_capcost',
    'temp.usp_ValueChain_Load_SaleSum',
    'temp.usp_ValueChain_Load_FinalAllocate',
    'temp.usp_ValueChain_Load_Locations',
    'ssis.usp_temp_CapCost_Pull_Delta',
    '[ssis].[usp_Modulus]',
    'ssis.usp_Fact_ValueChainSourceQuery',
    '[ssis].[usp_etl_Load_FactTableProcessExecution]',
    '[purgatory].[usp_ValueChain_MarkAsConsumed]'
]

vc_referenced_procs_dw = [
    '[ssis].[usp_purgatory_ValueChain_PreProcess]',
    'audit.FillFactValueChainAudit',
    'ssis.usp_Get_RowCount',

]
# Remove the brackets
vc_referenced_procs_dw = [proc.replace('[', '').replace(']', '') for proc in vc_referenced_procs_dw]
vc_referenced_procs_tl = [proc.replace('[', '').replace(']', '') for proc in vc_referenced_procs_tl]

references_query = """WITH src AS (
    SELECT
     [procedure] = QUOTENAME(s.name) + N'.' + QUOTENAME(o.name),
     ref =
             COALESCE(QUOTENAME(d.referenced_server_name) + N'.', N'')
             + COALESCE(QUOTENAME(d.referenced_database_name) + N'.', N'')
             + QUOTENAME(d.referenced_schema_name) + N'.'
             + QUOTENAME(d.referenced_entity_name)
    FROM sys.sql_expression_dependencies AS d
    INNER JOIN sys.objects AS o
    ON d.referencing_id = o.object_id
    INNER JOIN sys.schemas AS s
    ON o.schema_id = s.schema_id
    WHERE o.type = N'P'
), views AS (
    SELECT
        CONCAT(TABLE_SCHEMA, '.', TABLE_NAME) view_name
    FROM INFORMATION_SCHEMA.VIEWS
)
SELECT DISTINCT
    REPLACE(REPLACE([procedure], '[', ''), ']', '') stored_proc,
    REPLACE(REPLACE(ref, '[', ''), ']', '') ref
FROM src
LEFT OUTER JOIN views ON views.view_name = REPLACE(REPLACE(ref, '[', ''), ']', '')
WHERE views.view_name IS NULL AND
REPLACE(REPLACE([procedure], '[', ''), ']', '') IN (
{}
)"""

in_statement_tl = ''
proc_template = "\t'{}', \n"
for proc in vc_referenced_procs_tl:
    in_statement_tl += proc_template.format(proc)
in_statement_tl = in_statement_tl[:len(in_statement_tl) - 3]

in_statement_dw = ''
for proc in vc_referenced_procs_dw:
    in_statement_dw += proc_template.format(proc)
in_statement_dw = in_statement_dw[:len(in_statement_dw) - 3]

references_query_dw = references_query.format(in_statement_dw)
references_query_tl = references_query.format(in_statement_tl)

dw_dependencies = dbc.query(local_dw, references_query_dw)
tl_dependencies = dbc.query(local_tl, references_query_tl)

dw_dependencies['database'] = 'SpreeDW'
tl_dependencies['database'] = 'SpreeTL'

dw_schema_table = dw_dependencies['ref'].str.split(".", n=1, expand=True)
dw_dependencies['schema'] = dw_schema_table[0]
dw_dependencies['table'] = dw_schema_table[1]
dw_dependencies = dw_dependencies.drop(columns=['stored_proc', 'ref'])

tl_schema_table = tl_dependencies['ref'].str.split(".", n=1, expand=True)
tl_dependencies['schema'] = tl_schema_table[0]
tl_dependencies['table'] = tl_schema_table[1]
tl_dependencies = tl_dependencies.drop(columns=['stored_proc', 'ref'])

dependencies = tl_dependencies.append(dw_dependencies)
dependencies.drop_duplicates(inplace=True)
dependencies = dependencies.reset_index(drop=True)

# print(dependencies)

# Collect View Dependencies
references_query_views = """WITH src AS (
    SELECT
     [procedure] = QUOTENAME(s.name) + N'.' + QUOTENAME(o.name),
     ref =
             COALESCE(QUOTENAME(d.referenced_server_name) + N'.', N'')
             + COALESCE(QUOTENAME(d.referenced_database_name) + N'.', N'')
             + QUOTENAME(d.referenced_schema_name) + N'.'
             + QUOTENAME(d.referenced_entity_name)
    FROM sys.sql_expression_dependencies AS d
    INNER JOIN sys.objects AS o
    ON d.referencing_id = o.object_id
    INNER JOIN sys.schemas AS s
    ON o.schema_id = s.schema_id
    WHERE o.type = N'P'
), views AS (
    SELECT
        CONCAT(TABLE_SCHEMA, '.', TABLE_NAME) view_name
    FROM INFORMATION_SCHEMA.VIEWS
)
SELECT DISTINCT
    REPLACE(REPLACE([procedure], '[', ''), ']', '') stored_proc,
    REPLACE(REPLACE(ref, '[', ''), ']', '') ref
FROM src
LEFT OUTER JOIN views ON views.view_name = REPLACE(REPLACE(ref, '[', ''), ']', '')
WHERE views.view_name IS NOT NULL AND
REPLACE(REPLACE([procedure], '[', ''), ']', '') IN (
{}
)"""
vc_referenced_views_dw = dbc.query(local_dw, references_query_views.format(in_statement_dw))
vc_referenced_views_tl = dbc.query(local_tl, references_query_views.format(in_statement_tl))
vc_referenced_views_tl.drop(columns=['stored_proc'], inplace=True)
vc_referenced_views_tl.drop_duplicates(inplace=True)

view_dependencies_query = """SELECT DISTINCT
referenced_schema_name ,
referenced_entity_name
FROM sys.dm_sql_referenced_entities ('{}', 'OBJECT');"""

view_dependencies = pd.DataFrame(columns=['referenced_schema_name', 'referenced_entity_name'])

for index, table in vc_referenced_views_tl.iterrows():
    view_dependencies = view_dependencies.append(dbc.query(local_tl, view_dependencies_query.format(table['ref'])))
    view_dependencies.reset_index(drop=True)

view_dependencies.columns = ['schema', 'table']
view_dependencies['database'] = 'SpreeTL'

dependencies = dependencies.append(view_dependencies, sort=True).reset_index(drop=True)
print(dependencies)
