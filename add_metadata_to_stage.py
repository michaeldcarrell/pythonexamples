"""
Purpose: Loops over the stage tables in SpreeTL and creates the SQL statements to:
    * Add the additional metadata columns
    * Add a history table
    * Alter the stage table to use the history table.

Usage:

To execute this from the command line run:

python .\spreetl_helpers\add_metadata_to_stage.py --server {server} --database -- {database} --schema {schema} --path {file path}

If you'd like more verbose logging, add '-v'.  Be forewarned, it prints a lot of data...
"""
import argparse
import logging
import os
import pathlib
import typing
from copy import deepcopy
from functools import lru_cache, partial
from itertools import groupby

import pyodbc

SchemaTable = typing.NamedTuple(
    "SchemaTable",
    [
        ("schema", str),
        ("table", str),
        ("has_identity_flag", bool),
        ("has_primary_key", bool),
    ],
)
Column = typing.NamedTuple(
    "TableColumn",
    [
        ("column", str),
        ("data_type", str),
        ("is_identity", bool),
        ("is_primary_key", bool),
    ],
)

logger = logging.getLogger(__name__)

METADATA_COLUMNS = [
    Column("StatusTypeID", "int not null", False, False),
    Column("StateTypeID", "int not null", False, False),
    Column("RowHash", "varbinary(32)", False, False),
    Column("FirstSeenOn", "datetimeoffset", False, False),
    Column("LastSeenOn", "datetimeoffset", False, False),
    Column("Stage{table_clean}ID", "int not null", True, False),
    Column("ValidFrom", "datetime2(0) not null", False, False),
    Column("ValidTill", "datetime2(0) not null", False, False),
]

IDENTITY_COLUMN = Column("Stage{table_clean}ID", "int not null", True, False)


def connect_db(server: str, database: str) -> pyodbc.Connection:
    """
    Opens a database connection and returns the connection.
    """
    cnxn_string = (
        "Driver={ODBC Driver 17 for SQL Server};"
        + "Server={};Database={};Trusted_Connection=yes".format(server, database)
    )
    return pyodbc.connect(cnxn_string)


def get_table_list(
    db_connection: pyodbc.Connection, schema: str
) -> typing.List[SchemaTable]:
    """
    Returns a list of tables that need to be altered.
    """
    sql_stmt = """
        WITH cte_existing_identity_column
            AS (SELECT DISTINCT c.TABLE_SCHEMA, c.TABLE_NAME
                FROM INFORMATION_SCHEMA.COLUMNS c
                WHERE COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1
                    AND c.COLUMN_NAME NOT LIKE 'Stage%')
            SELECT t.TABLE_SCHEMA AS [schema], 
                    t.TABLE_NAME AS [table], 
                    (CASE
                        WHEN ceic.TABLE_NAME IS NOT NULL
                        THEN 1
                        ELSE 0
                    END) [has_identity_flag],
                    (CASE WHEN tc.TABLE_NAME IS NOT NULL THEN 1 ELSE 0 end) [has_primary_key]
            FROM INFORMATION_SCHEMA.TABLES t
                LEFT JOIN cte_existing_identity_column AS ceic ON t.TABLE_NAME = ceic.TABLE_NAME AND t.TABLE_SCHEMA = ceic.TABLE_SCHEMA
                LEFT JOIN information_schema.TABLE_CONSTRAINTS tc ON t.TABLE_SCHEMA = tc.TABLE_SCHEMA AND t.TABLE_NAME = tc.TABLE_NAME AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
            WHERE t.TABLE_TYPE = 'BASE TABLE'
                AND t.TABLE_SCHEMA = ?
            ORDER BY t.TABLE_NAME;
    """
    return [SchemaTable(*elem) for elem in db_connection.execute(sql_stmt, schema)]


def get_table_columns(
    db_connection: pyodbc.Connection, schema: str
) -> typing.Dict[SchemaTable, typing.List[Column]]:
    """
    Fetches table table columns for the schema, and then builds a dictionary where the key is the SchemaTable and the value is a list of columns.

    :param db_connection: database connection
    :param schema: database schema
    """
    sql_stmt = """
        SELECT c.TABLE_SCHEMA AS TableSchema, 
                c.TABLE_NAME AS TableName, 
                '[' + c.COLUMN_NAME + ']' AS ColumnName,
                c.DATA_TYPE + (CASE c.DATA_TYPE
                                    WHEN 'sql_variant'
                                    THEN ''
                                    WHEN 'text'
                                    THEN ''
                                    WHEN 'ntext'
                                    THEN ''
                                    WHEN 'xml'
                                    THEN ''
                                    WHEN 'decimal'
                                    THEN '(' + CAST(numeric_precision AS VARCHAR) + ', ' + CAST(numeric_scale AS VARCHAR) + ')'
									WHEN 'numeric'
                                    THEN '(' + CAST(numeric_precision AS VARCHAR) + ', ' + CAST(numeric_scale AS VARCHAR) + ')'
                                    ELSE COALESCE('(' + CASE
                                                            WHEN character_maximum_length = -1
                                                            THEN 'MAX'
                                                            ELSE CAST(character_maximum_length AS VARCHAR)
                                                        END + ')', '')
                                END) + (CASE WHEN c.IS_NULLABLE = 'YES' THEN ' null' ELSE ' not null' END) AS SQLDataTypeStmt,
                COLUMNPROPERTY(object_id(c.TABLE_SCHEMA+'.'+ c.TABLE_NAME), c.COLUMN_NAME, 'IsIdentity') AS IsIdentity,
                (CASE WHEN ccu.COLUMN_NAME IS NOT NULL THEN 1 ELSE 0 end) AS IsPrimaryKey
            FROM INFORMATION_SCHEMA.COLUMNS c
                LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON c.TABLE_SCHEMA = ccu.TABLE_SCHEMA AND c.TABLE_NAME = ccu.TABLE_NAME AND c.COLUMN_NAME = ccu.COLUMN_NAME
            WHERE c.TABLE_SCHEMA = ?
            AND (EXISTS (
                SELECT 1
                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                WHERE tc.TABLE_NAME = ccu.TABLE_NAME
                    AND tc.TABLE_SCHEMA = ccu.TABLE_SCHEMA
                    AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY') OR 1 = 1)
            ORDER BY c.TABLE_NAME, 
                     c.ORDINAL_POSITION;
        """
    data = db_connection.execute(sql_stmt, schema).fetchall()
    ret_data = {}  # type: typing.Dict[SchemaTable, typing.List[Column]]
    for k, g in groupby(data, key=lambda x: (x[0], x[1])):
        ret_data.update({k: [Column(x[2], x[3], False, False) for x in g]})
    return ret_data


def clean_table_name(schema_table: SchemaTable) -> str:
    """
    Helper function to clean the table name.
    """
    return schema_table.table.replace("_", "")


def generate_stage_alter(schema_table: SchemaTable) -> str:
    """
    Generates an ALTER TABLE statement that will add: 
        * StatusTypeID
        * StateTypeID
        * RowHash
        * FirstSeenOn
        * LastSeenOn
        * Stage{tabke}ID
        * ValidFrom
        * ValidTo
    """

    if not schema_table.has_identity_flag:
        identity_column = "Stage{table_clean}ID INT IDENTITY(1, 1),".format(
            table_clean=clean_table_name(schema_table=schema_table)
        )
    else:
        identity_column = ""

    alter_table_stmt = """
        ALTER TABLE {schema}.{table}
        ADD StatusTypeID INT NOT NULL DEFAULT 2,
            StateTypeID INT NOT NULL DEFAULT 0,
            RowHash VARBINARY(32),
            FirstSeenOn DATETIMEOFFSET,
            LastSeenOn DATETIMEOFFSET,
            {identity_column}
            ValidFrom datetime2(0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT {schema}_{table}_ValidFrom DEFAULT SYSDATETIME(),
            ValidTill datetime2(0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT {schema}_{table}_ValidTill DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
            PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTill);
        GO
    """
    return alter_table_stmt.format(
        schema=schema_table.schema,
        table=schema_table.table,
        identity_column=identity_column,
    )


def get_primary_key_column(db_connection: pyodbc.Connection, schema_table: SchemaTable):
    """
    Resolves primary key column based on (1) identity column or (2) newly created identity column
    """
    if schema_table.has_identity_flag:
        # fetch identity from db
        column_name = db_connection.execute(
            """
        SELECT c.COLUMN_NAME AS ColumnName
         FROM information_schema.columns c
        WHERE c.TABLE_SCHEMA = ?
          AND c.TABLE_NAME = ?
          AND COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1;
        """,
            schema_table.schema,
            schema_table.table,
        ).fetchone()[0]
    else:
        column_name = "Stage{table_clean}ID".format(
            table_clean=clean_table_name(schema_table=schema_table)
        )
    return column_name


def generate_stage_add_constraint(
    schema_table: SchemaTable, primary_key_generator: typing.Callable
) -> str:
    """
    Generates the ALTER TABLE statement to add the primary key
    """
    alter_table_stmt = ""

    if not schema_table.has_primary_key:
        column_name = primary_key_generator(schema_table=schema_table)
        alter_table_stmt = """
        ALTER TABLE {schema}.{table} ADD CONSTRAINT PK_{table} PRIMARY KEY ({primary_key});
        GO
        """.format(
            schema=schema_table.schema,
            table=schema_table.table,
            primary_key=column_name,
        )

    return alter_table_stmt


def generate_history_table_ddl(
    schema_table: SchemaTable, table_columns: typing.List[Column]
) -> str:
    """
    Generates history table ddl for a given table.  If the history table doesn't need to have the identity column, then we remove it from the metadata_columns.

    :param schema_table: table object
    :table_columns: list of columsn in the table.
    """
    metadata_columns = deepcopy(METADATA_COLUMNS)
    if schema_table.has_identity_flag:
        metadata_columns.pop(metadata_columns.index(IDENTITY_COLUMN))

    history_columns = table_columns + metadata_columns

    history_columns_ddl = ",\n".join(
        [
            "\t\t\t\t\t\t\t\t{} {}".format(elem.column, elem.data_type)
            for elem in history_columns
        ]
    ).format(table_clean=schema_table.table.replace("_", ""))

    create_table_ddl = """
        CREATE TABLE history.{schema_title}{table}History (
            {columns}
        );
        GO

        ALTER TABLE {schema}.{table}
          SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = history.{schema_title}{table}History));
        GO
    """.format(
        schema=schema_table.schema,
        schema_title=schema_table.schema.title(),
        table=schema_table.table,
        columns=history_columns_ddl,
    )

    return create_table_ddl


def generate_sql_stmts(
    schema_tables: typing.List[SchemaTable],
    table_columns: typing.Dict[SchemaTable, typing.List[Column]],
    primary_key_generator: typing.Callable,
) -> typing.List[str]:
    """
    Generates a list of alter statements for a given set of tables.

    :param schema_table: table we are generating the SQL statements for
    :param table_columns: list of columns that the table has_identity_flag
    :param primary_key_generator: callable that will generate the primary key column name when passed a schema_table obeject.
    """
    sql_stmts = []
    for schema_table in schema_tables:
        logger.info(
            "Generating ALTER commands for {}.{}".format(
                schema_table.schema, schema_table.table
            )
        )
        columns = table_columns[(schema_table.schema, schema_table.table)]
        sql_stmts.append(generate_stage_alter(schema_table))
        sql_stmts.append(
            generate_stage_add_constraint(
                schema_table, primary_key_generator=primary_key_generator
            )
        )
        sql_stmts.append(generate_history_table_ddl(schema_table, columns))
    return sql_stmts


def parse_args() -> typing.Tuple[str, str, str, pathlib.Path, bool]:
    """
    Parses the arguments passed to the CLI
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--server", required=True, type=str, help="server name of the database"
    )

    parser.add_argument("--database", required=True, type=str, help="database name")

    parser.add_argument("--schema", required=True, type=str, help="schema name")

    parser.add_argument(
        "--path", required=True, type=pathlib.Path, help="Path to the file output"
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        required=False,
        help="verbose logging output (sets log level to DEBUG)",
    )

    args = parser.parse_args()

    return (args.server, args.database, args.schema, args.path, args.verbose)


def main(db_connection: pyodbc.Connection, schema: str, output_file: pathlib.Path):
    """
    Main method to execute through the CLI.

    :param db_connection: connection to the database
    :param schema: schema name
    :param output_file: output file for where to generate the SQL statements
    """
    logging.info(
        "Processing request to generate metadata adds for schema:{}".format(schema)
    )

    # generator a partial function to pass into generate_sql_stms so we're not passing the connection around.
    primary_key_generator = partial(get_primary_key_column, db_connection=db_connection)

    schema_tables = get_table_list(db_connection=db_connection, schema=schema)
    table_columns = get_table_columns(db_connection=db_connection, schema=schema)
    sql_stmts = generate_sql_stmts(
        schema_tables=schema_tables,
        table_columns=table_columns,
        primary_key_generator=primary_key_generator,
    )

    with output_file.open("w") as f:
        for stmt in sql_stmts:
            logger.debug("writing: {}".format(stmt))
            _ = f.write(stmt)


if __name__ == "__main__":
    server, database, schema, path, verbose_logging = parse_args()

    logging.basicConfig(
        level=logging.DEBUG if verbose_logging else logging.INFO,
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[logging.StreamHandler()],
    )

    logger.info(
        "Opening db connection to server: {} and database: {}".format(server, database)
    )
    db_connection = connect_db(server=server, database=database)

    main(db_connection=db_connection, schema=schema, output_file=path)
