import pyperclip as clipboard
index_str = """IF INDEXPROPERTY(OBJECT_ID('m2_dbo.Sales_Values'), 'Clustered_ColumnStore_Sales_Values', 'IndexID') IS NULL
CREATE CLUSTERED COLUMNSTORE INDEX Clustered_ColumnStore_Sales_Values ON m2_dbo.Sales_Values;
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_amz_Feedback_RowHash ON m2_dbo.amz_Feedback(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_cr_ContactCategories_ContactCategoryID ON m2_dbo.cr_ContactCategories(ContactCategoryID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_cr_ContactCategories_RowHash ON m2_dbo.cr_ContactCategories(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Adjustments_AdjustmentID ON m2_dbo.dp_Adjustments(AdjustmentID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Adjustments_LocationID_AdjustmentID ON m2_dbo.dp_Adjustments(PkgItemID ASC, LocationID ASC, AdjustmentID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Adjustments_RowHash ON m2_dbo.dp_Adjustments(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Boxes_BoxID ON m2_dbo.dp_Boxes(BoxID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Boxes_RowHash ON m2_dbo.dp_Boxes(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Exceptions_ExceptionTypeID ON m2_dbo.dp_Exceptions(ExceptionTypeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Exceptions_RowHash ON m2_dbo.dp_Exceptions(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_ExceptionTracking_EXID ON m2_dbo.dp_ExceptionTracking(EXID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_ExceptionTracking_RowHash ON m2_dbo.dp_ExceptionTracking(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Origins_OriginID ON m2_dbo.dp_Origins(OriginID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Origins_RowHash ON m2_dbo.dp_Origins(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Origins_WarehouseZip ON m2_dbo.dp_Origins(WarehouseZip ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PackageItems_ItemID ON m2_dbo.dp_PackageItems(ItemID ASC) INCLUDE(PkgItemID);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PackageItems_PkgItemID ON m2_dbo.dp_PackageItems(PkgItemID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PackageItems_RowHash ON m2_dbo.dp_PackageItems(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PackageItems_SaleID_ItemID_PackageID ON m2_dbo.dp_PackageItems(SaleID ASC, ItemID ASC, PackageID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_packageItemsSaleID ON m2_dbo.dp_PackageItems(SaleID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Packages_Cancelled ON m2_dbo.dp_Packages(Cancelled ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Packages_PackageID ON m2_dbo.dp_Packages(PackageID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Packages_RowHash ON m2_dbo.dp_Packages(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Printers_PrinterID ON m2_dbo.dp_Printers(PrinterID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Printers_RowHash ON m2_dbo.dp_Printers(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Printers_WarehouseID ON m2_dbo.dp_Printers(WarehouseID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PrintQueue_PackageID_PrinterID ON m2_dbo.dp_PrintQueue(PackageID ASC, PrinterID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PrintQueue_QueueID ON m2_dbo.dp_PrintQueue(QueueID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_PrintQueue_RowHash ON m2_dbo.dp_PrintQueue(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Shipments_OriginID_ShippingAddressID ON m2_dbo.dp_Shipments(OriginID ASC, ShippingAddressID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Shipments_RowHash ON m2_dbo.dp_Shipments(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Shipments_ShipmentID ON m2_dbo.dp_Shipments(ShipmentID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_TransitTimes_OriginZip_DestZip_TransitRank ON m2_dbo.dp_TransitTimes(OriginZip ASC, DestZip ASC, TransitRank ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_TransitTimes_RowHash ON m2_dbo.dp_TransitTimes(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_TransitTimes_TransitTimeID ON m2_dbo.dp_TransitTimes(TransitTimeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Validation_RowHash ON m2_dbo.dp_Validation(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_dp_Validation_ValidID ON m2_dbo.dp_Validation(ValidID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_FeedbackHistory_FBHistID ON m2_dbo.FeedbackHistory(FBHistID ASC, SaleID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_FeedbackHistory_RowHash ON m2_dbo.FeedbackHistory(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_FeedbackStatusLog_FeedbackID ON m2_dbo.FeedbackStatusLog(FeedbackID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_FeedbackStatusLog_FeedbackStatusLogID ON m2_dbo.FeedbackStatusLog(FeedbackStatusLogID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_FeedbackStatusLog_RowHash ON m2_dbo.FeedbackStatusLog(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Inventory_Warehouses_RowHash ON m2_dbo.Inventory_Warehouses(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Inventory_Warehouses_WarehouseID ON m2_dbo.Inventory_Warehouses(WarehouseID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_InventoryItemIDMapping_MapID ON m2_dbo.InventoryItemIDMapping(MapID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_InventoryItemIDMapping_RowHash ON m2_dbo.InventoryItemIDMapping(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_InventoryLocations_LocationID ON m2_dbo.InventoryLocations(LocationID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_InventoryLocations_RowHash ON m2_dbo.InventoryLocations(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ExtendedProperties_ItemID ON m2_dbo.items_ExtendedProperties(ItemID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ExtendedProperties_RowHash ON m2_dbo.items_ExtendedProperties(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_InventoryStatusChanges_StatusChangeID ON m2_dbo.items_InventoryStatusChanges(StatusChangeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_InventoryStatusType_InventoryStatusID ON m2_dbo.items_InventoryStatusType(InventoryStatusID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ItemStatusChanges_ChangeID ON m2_dbo.items_ItemStatusChanges(ChangeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ItemStatusChanges_DateChanged ON m2_dbo.items_ItemStatusChanges(DateChanged ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ItemStatusChanges_RowHash ON m2_dbo.items_ItemStatusChanges (RowHash);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_Manufacturers_ManufID ON m2_dbo.items_Manufacturers(ManufID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_Manufacturers_RowHash ON m2_dbo.items_Manufacturers(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_MarketplaceType_ID ON m2_dbo.items_MarketplaceType(ID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_MarketplaceType_RowHash ON m2_dbo.items_MarketplaceType(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ProcurementStatusChanges_DateChanged ON m2_dbo.items_ProcurementStatusChanges(DateChanged ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ProcurementStatusChanges_RowHash ON m2_dbo.items_ProcurementStatusChanges(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ProcurementStatusChanges_StatusChangeID ON m2_dbo.items_ProcurementStatusChanges(StatusChangeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ProcurementStatusType_ProcurementStatusID ON m2_dbo.items_ProcurementStatusType(ProcurementStatusID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_ProcurementStatusType_RowHash ON m2_dbo.items_ProcurementStatusType(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_Promotions_PromoID ON m2_dbo.items_Promotions(PromoID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_Promotions_RowHash ON m2_dbo.items_Promotions(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_PromotionStatusType_RowHash ON m2_dbo.items_PromotionStatusType(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_items_PromotionStatusType_StatusID ON m2_dbo.items_PromotionStatusType(StatusID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_LSInventory_AdjustmentID ON m2_dbo.LSInventory(AdjustmentID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_LSInventory_ItemID ON m2_dbo.LSInventory(ItemID ASC) INCLUDE(AdjustmentDate);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_LSInventory_RelIDType_RelID ON m2_dbo.LSInventory(RelIDType ASC, RelID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_LSInventory_RowHash ON m2_dbo.LSInventory(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_MarketplaceRefunds_RefundID ON m2_dbo.MarketplaceRefunds(RefundID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_MarketplaceRefunds_RowHash ON m2_dbo.MarketplaceRefunds(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_mm_MailCases_CaseID ON m2_dbo.mm_MailCases(CaseID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_mm_MailCases_RowHash ON m2_dbo.mm_MailCases(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_mm_MailQueues_MailQueueID ON m2_dbo.mm_MailQueues(MailQueueID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_mm_MailQueues_RowHash ON m2_dbo.mm_MailQueues(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_PayPalRefunds_RefundID ON m2_dbo.PayPalRefunds(RefundID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_PayPalRefunds_RowHash ON m2_dbo.PayPalRefunds(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_Adjustments_AdjustmentID ON m2_dbo.rcv_Adjustments(AdjustmentID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_Adjustments_POID ON m2_dbo.rcv_Adjustments(POID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_Adjustments_RowHash ON m2_dbo.rcv_Adjustments(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POItems_POID_ItemID_SellThroughDate ON m2_dbo.rcv_POItems(POID ASC, ItemID ASC, SellThroughDate ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POItems_POItemID ON m2_dbo.rcv_POItems(POItemID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POItems_RowHash ON m2_dbo.rcv_POItems(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POs_ImportedFromOrderPicking ON m2_dbo.rcv_POs(ImportedFromOrderPicking ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POs_POID ON m2_dbo.rcv_POs(POID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POs_RowHash ON m2_dbo.rcv_POs(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POStatusChanges_RowHash ON m2_dbo.rcv_POStatusChanges(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_POStatusChanges_StatusChangeID ON m2_dbo.rcv_POStatusChanges(StatusChangeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_Status_RowHash ON m2_dbo.rcv_Status(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_rcv_Status_StatusID ON m2_dbo.rcv_Status(StatusID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_ReturnItemAction_ReturnItemActionID ON m2_dbo.ReturnItemAction(ReturnItemActionID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_ReturnItemAction_RowHash ON m2_dbo.ReturnItemAction(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAItems_RMAID_ItemID_RMAItemTypeID ON m2_dbo.RMAItems(RMAID ASC, ItemID ASC, RMAItemTypeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAItems_RMAItemID ON m2_dbo.RMAItems(RMAItemID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAItems_RowHash ON m2_dbo.RMAItems(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMARequestTypes_RequestTypeID ON m2_dbo.RMARequestTypes(RequestTypeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMARequestTypes_RowHash ON m2_dbo.RMARequestTypes(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAs_RmaID ON m2_dbo.RMAs(RmaID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAs_RowHash ON m2_dbo.RMAs(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAs_SaleID ON m2_dbo.RMAs(SaleID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAs_SecondLevelResponseID ON m2_dbo.RMAs(SecondLevelResponseID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAsProductStatusTypes_ProductStatusID ON m2_dbo.RMAsProductStatusTypes(ProductStatusID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_RMAsProductStatusTypes_RowHash ON m2_dbo.RMAsProductStatusTypes(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_s_ShippingCo_RowHash ON m2_dbo.s_ShippingCo(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_s_ShippingCo_ShippingCoID ON m2_dbo.s_ShippingCo(ShippingCoID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_s_TrackingNumbers_RowHash ON m2_dbo.s_TrackingNumbers(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_s_TrackingNumbers_TrkID ON m2_dbo.s_TrackingNumbers(TrkID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Sale_Values_SaleID_ValueTypeID_Sales_Values ON m2_dbo.Sales_Values(SaleID ASC, ValueTypeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Sales_Values_RankID ON m2_dbo.Sales_Values(RankID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Sales_Values_RowHash ON m2_dbo.Sales_Values(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Sales_Values_valueID ON m2_dbo.Sales_Values(valueID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SecondLevelResponse_RowHash ON m2_dbo.SecondLevelResponse(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SecondLevelResponse_SecondLevelResponseID ON m2_dbo.SecondLevelResponse(SecondLevelResponseID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SecondLevelResponseReportGroup_ReportGroupID ON m2_dbo.SecondLevelResponseReportGroup(ReportGroupID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SecondLevelResponseReportGroup_RowHash ON m2_dbo.SecondLevelResponseReportGroup(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturnItems_ItemID ON m2_dbo.SupplierReturnItems(ItemID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturnItems_RowHash ON m2_dbo.SupplierReturnItems(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturns_RAID ON m2_dbo.SupplierReturns(RAID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturns_RowHash ON m2_dbo.SupplierReturns(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturnStatus_RowHash ON m2_dbo.SupplierReturnStatus(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturnStatus_StatusID ON m2_dbo.SupplierReturnStatus(StatusID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturnStatusChanges_RowHash ON m2_dbo.SupplierReturnStatusChanges(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_SupplierReturnStatusChanges_SupplierReturnStatusChangeID ON m2_dbo.SupplierReturnStatusChanges(SupplierReturnStatusChangeID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Users_RowHash ON m2_dbo.Users(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Users_UserID ON m2_dbo.Users(UserID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Website_Coupons_ID ON m2_dbo.Website_Coupons(ID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Website_Coupons_RowHash ON m2_dbo.Website_Coupons(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Website_Coupons_Scenarios_ID ON m2_dbo.Website_Coupons_Scenarios(ID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Website_Coupons_Scenarios_RowHash ON m2_dbo.Website_Coupons_Scenarios(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Website_Coupons_Sources_ID ON m2_dbo.Website_Coupons_Sources(ID ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_Website_Coupons_Sources_RowHash ON m2_dbo.Website_Coupons_Sources(RowHash ASC);
IF INDEXPROPERTY(OBJECT_ID('m2_dbo.table'), 'index', 'IndexID') IS NULL
CREATE NONCLUSTERED INDEX IX_m2_dbo_amz_Feedback_FBID ON m2_dbo.amz_Feedback(FBID ASC, SaleID ASC);"""
counter = 0
index_property_template = "IF INDEXPROPERTY(OBJECT_ID('{}.{}'), '{}', 'IndexID') IS NULL\n\t"
statement = ''
for line in index_str.split('\n'):
    if counter % 2 == 1:
        index_values = line.split('INDEX')[1].split(' ON ')
        index_name = index_values[0].replace(' ', '')
        table_and_column = index_values[1].split('(')
        table_and_schema = table_and_column[0].split('.')
        table = table_and_schema[1].replace(';', '')
        schema = table_and_schema[0]
        statement += index_property_template.format(schema, table, index_name) + line + '\n'
    counter += 1
print(statement)
clipboard.copy(statement)
