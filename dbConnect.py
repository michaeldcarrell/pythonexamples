import pyodbc as odbc
import pandas as pd
import datetime as dt
import getpass


def server_connect(server, database):
    connection = odbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                              f'Server={server}.lapkosoft.local;'
                              f'Database={database};'
                              'Trusted_Connection=yes')

    return connection


def query(connection, sql):
    return pd.read_sql(sql, connection)


def statement(connection, sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    cursor.commit()


def databases_from_server(server):
    databases = query(server_connect(server, 'master'), "SELECT name Databases FROM sys.databases")
    return databases


def schemas_from_database(server, database):
    schemas = query(server_connect(server, database), "SELECT name Schemas FROM sys.schemas")
    return schemas


def tables_from_schema(server, database, schema):
    tables = query(server_connect(server, database),
                   f"SELECT TABLE_NAME Tables FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{schema}'")
    return tables


def column_structure_tables(table_name, column_structure, stage_key):
    column_block = ''
    counter = 0
    for column in column_structure.iterrows():
        if counter != 0:
            newline = '\n'
        else:
            newline = ''
        column_block += f'{newline}\t' + column[1]['Columns'] + ' ' + column[1]['Data_Types'] + ', '
        counter += 1

    row_hash_block = ''
    for column in column_structure.iterrows():
        row_hash_block += f"CONVERT(NVARCHAR, {column[1]['Columns']}) + '|' + "

    row_hash = f"RowHash AS CONVERT(VARBINARY(32), HASHBYTES('SHA2_256', " +\
               row_hash_block[:len(row_hash_block) - 9] + ")) PERSISTED"

    create_stage_table_sql = f"""
CREATE TABLE stage.{table_name} (
    {stage_key} INT IDENTITY(1,1),
{column_block}
    IsIncrementalInsertOverlap BIT,
    IsSourceSystemActive BIT,
    SourceTableProcessExecutionID BIGINT,
    StatusTypeID INT NOT NULL DEFAULT 2,
    StateTypeID INT NOT NULL DEFAULT 0,
    RowHash VARBINARY(32),
    FirstSeenOn DATETIMEOFFSET,
    LastSeenOn DATETIMEOFFSET,
    ValidFrom datetime2(0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT DF_ValidFrom DEFAULT SYSDATETIME(),
    ValidTill datetime2(0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT DF_ValidTill DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
    PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTill)
);
ALTER TABLE stage.{table_name} ADD CONSTRAINT PK_{table_name} PRIMARY KEY ({stage_key});
"""
    create_etldata_table_sql = f"""
CREATE TABLE etldata.{table_name} (
{column_block}
    ChangeOperation NCHAR(1),
    SourceTableProcessExecutionID BIGINT,
    StatusTypeID INT NOT NULL DEFAULT 2,
    StateTypeID INT NOT NULL DEFAULT 0,
    {row_hash}
);
"""
    create_history_table_sql = f"""
CREATE TABLE history.Stage{table_name}History (
    {stage_key} INT,
{column_block}
    IsIncrementalInsertOverlap BIT null,
    IsSourceSystemActive BIT null,
    SourceTableProcessExecutionID BIGINT null,
    StateTypeID INT NOT null,
    StatusTypeID INT NOT null,
    RowHash VARBINARY(32) null,
    FirstSeenOn DATETIMEOFFSET(7) null,
    LastSeenOn DATETIMEOFFSET(7) null,
    ValidFrom DATETIME2(0) NOT NULL,
    ValidTill DATETIME2(0) NOT NULL
);

ALTER TABLE stage.{table_name}
  SET (SYSTEM_VERSIONING    = ON (HISTORY_TABLE = history.Stage{table_name}History));
"""
    return {'stage': {'sql': create_stage_table_sql, 'table_name': f"stage.{table_name}"},
            'etldata': {'sql': create_etldata_table_sql, 'table_name': f"etldata.{table_name}"},
            'history': {'sql': create_history_table_sql, 'table_name': f"history.Stage{table_name}History"}}


def check_table_exists_tl(table, schema):
    stage_table_exists_sql = f"""
    SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_NAME = '{table}'
    AND TABLE_SCHEMA = '{schema}'
    """

    stage_table_exists = query(server_connect('azprod-bi-sql', 'SpreeTL_Dev'), stage_table_exists_sql)
    if len(stage_table_exists) == 1:
        return True
    else:
        return False


def column_structure_sp(table_name, primary_key):
    column_structure_sql = f"""
SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = '{table_name}'
AND TABLE_SCHEMA = 'etldata'
"""
    column_structure = query(server_connect('azprod-bi-sql', 'SpreeTL_Dev'), column_structure_sql)
    column_block = ''
    counter = 0
    for column in column_structure.iterrows():
        if column[1]['COLUMN_NAME'] not in ['StatusTypeID', 'StateTypeID', 'ChangeOperation']:
            if counter != 0:
                newline = '\n'
            else:
                newline = ''
            column_block += f'{newline}\t\t' + column[1]['COLUMN_NAME'] + ', '
            counter += 1

    column_block_etldata = ''
    counter = 0
    for column in column_structure.iterrows():
        if column[1]['COLUMN_NAME'] not in ['StatusTypeID', 'StateTypeID', 'ChangeOperation']:
            if counter != 0:
                newline = '\n'
            else:
                newline = ''
            column_block_etldata += f'{newline}\t\tetldata.' + column[1]['COLUMN_NAME'] + ', '
            counter += 1

    column_block_update = ''
    counter = 0
    for column in column_structure.iterrows():
        if column[1]['COLUMN_NAME'] not in ['StateTypeID', 'StatusTypeID', 'ChangeOperation']:
            if counter != 0:
                newline = '\n'
            else:
                newline = ''
            column_block_update += f'{newline}\t\t' + column[1]['COLUMN_NAME'] + ' = etldata.' + column[1][
                'COLUMN_NAME'] + ', '
            counter += 1

    sp_sql_status_state = f"""-- =====================================================================================================================
-- Author: <DataInsights Team ({getpass.getuser()})>
-- Creation Date: <{dt.date.today()}>
-- Description: <This stored procedure will update status states for the {table_name} table>
-- =====================================================================================================================

CREATE PROCEDURE ssis.usp_etldata_{table_name}_UpdateStatusState
    @SourceTableProcessExecutionID BIGINT,
    @StatusTypeID INT = 1,
    @StateTypeID INT = 1

AS

BEGIN
    SET NOCOUNT ON;

    UPDATE etldata.{table_name}
    SET
        StateTypeID = @StateTypeID,
        StatusTypeID = @StatusTypeID
    WHERE {table_name}.SourceTableProcessExecutionID = @SourceTableProcessExecutionID
    SET NOCOUNT OFF;
END
"""

    sp_sql_delete = f"""-- =====================================================================================================================
-- Author: <DataInsights Team ({getpass.getuser()})>
-- Creation Date: <{dt.date.today()}>
-- Description: <This stored procedure will match deleted from source table {table_name} table>
-- =====================================================================================================================

CREATE PROCEDURE ssis.usp_etldata_{table_name}_Delete 
    @SourceTableProcessExecutionID BIGINT

AS
BEGIN
    SET NOCOUNT ON;
    DELETE FROM etldata.{table_name}
    WHERE {table_name}.SourceTableProcessExecutionID = @SourceTableProcessExecutionID;
    SET NOCOUNT OFF;
END;
"""

    sp_sql_merge = f"""-- =====================================================================================================================
-- Author: <DataInsights Team ({getpass.getuser()})>
-- Creation Date: <{dt.date.today()}>
-- Description: <This stored procedure will merge changes from source table {table_name}>
-- =====================================================================================================================

CREATE PROCEDURE ssis.usp_Merge_stage_{table_name}
    @SourceTableProcessExecutionID BIGINT

AS
BEGIN
    SET NOCOUNT ON;
    INSERT INTO stage.{table_name} (
{column_block}
        IsIncrementalInsertOverlap,
        IsSourceSystemActive,
        StatusTypeID,
        StateTypeID,
        FirstSeenOn,
        LastSeenOn
    )
    SELECT
{column_block_etldata}
        0 IsIncrementalInsertOverlap,
        1 IsSourceSystemActive,
        1 StatusTypeID,
        1 StateTypeID,
        SYSDATETIMEOFFSET() FirstSeenOn,
        SYSDATETIMEOFFSET() LastSeenOn
    FROM etldata.{table_name} etldata
    LEFT OUTER JOIN stage.{table_name} stage ON etldata.RowHash = stage.RowHash
    WHERE etldata.ChangeOperation IN ('I', 'U')
    AND stage.RowHash IS NULL
    AND etldata.StateTypeID = 1
    AND etldata.StatusTypeID = 1
    AND etldata.SourceTableProcessExecutionID = @SourceTableProcessExecutionID
    AND NOT EXISTS (
        SELECT 1
        FROM stage.{table_name} a1
        WHERE a1.{primary_key} = etldata.{primary_key}
    );

    UPDATE stage
    SET
        LastSeenOn = SYSDATETIMEOFFSET()
    FROM stage.{table_name} stage
    INNER JOIN etldata.{table_name} etldata ON (stage.{primary_key} = etldata.{primary_key} AND
                                                    stage.RowHash != etldata.RowHash);

    UPDATE stage
    SET
{column_block_update}
        LastSeenOn = SYSDATETIMEOFFSET(),
        StatusTypeID = 1,
        StateTypeID = 4
    FROM stage.{table_name} stage
    INNER JOIN etldata.{table_name} etldata ON (stage.{primary_key} = etldata.{primary_key} AND
                                            stage.RowHash != etldata.RowHash)
    WHERE etldata.ChangeOperation IN('I', 'U');

    UPDATE stage
    SET
        IsSourceSystemActive = 0,
        SourceTableProcessExecutionID = @SourceTableProcessExecutionID,
        StatusTypeID = 3,
        StateTypeID = 4
    FROM stage.{table_name} stage
    INNER JOIN etldata.{table_name} etldata ON (stage.{primary_key} = etldata.{primary_key})
    WHERE etldata.ChangeOperation = 'D';
    SET NOCOUNT OFF;
END
    """
    return {'status_state': {'sql': sp_sql_status_state, 'proc_name': f'usp_etldata_{table_name}_UpdateStatusState'},
            'delete': {'sql': sp_sql_delete, 'proc_name': f'usp_etldata_{table_name}_Delete '},
            'merge': {'sql': sp_sql_merge, 'proc_name': f'usp_Merge_stage_{table_name}'}}


def column_structure_ssis(column_structure, schema_name, table_name, primary_key):
    column_block = ''
    counter = 0
    for column in column_structure.iterrows():
        if column[1]['Columns'] not in [primary_key]:
            if counter != 0:
                newline = '\n'
            else:
                newline = ''
            column_block += f'{newline}\t' + column[1]['Columns'] + ', '
            counter += 1

    column_block_source = ''
    counter = 0
    for column in column_structure.iterrows():
        if column[1]['Columns'] not in [primary_key]:
            if counter != 0:
                newline = '\n'
            else:
                newline = ''
            column_block_source += f'{newline}\tsource.' + column[1]['Columns'] + ', '
            counter += 1

    ssis_statement = f'''( @[$Package::ProcessTypeName]  =="Full Load")?
"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT "+(@[$Package::TestingMode]?" TOP 100 ":"")+"
	{primary_key},--Primary Key
    /***BEGIN Non-PK fields for this table***/
{column_block}
    /***END Non-PK fields for this table***/	
    CAST('I' AS NCHAR(1)) AS [ChangeOperation]
FROM {schema_name}.{table_name}
WHERE 1=1
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
":"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
    source_changetracking.{primary_key}, --Primary Key
    /***BEGIN Non-PK fields for this table***/
{column_block_source}
    /***END Non-PK fields for this table***/
    CAST(source_changetracking.SYS_CHANGE_OPERATION AS NCHAR(1)) AS ChangeOperation
FROM ChangeTable(CHANGES {schema_name}.{table_name}, "+(DT_WSTR,20)@[template::ChangeTrackingCurrentVersionForCurrentLoad]+") source_changetracking
INNER JOIN {schema_name}.{table_name} source ON source_changetracking.{primary_key} = source.{primary_key}
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
"
'''
    return ssis_statement
