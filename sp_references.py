import create_stage_tables as cst
import json

if __name__ == "__main__":
    staticTables = [
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'Date'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'DateEvent'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'DetailedCOGSCategory'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'ItemGrade'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'ItemGradeSpread'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'PaymentType'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'ReturnItemAction'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'RMARequestType'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'SaleStatus'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'SecondLevelResponse'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'SupplyChainStatus'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'Time'},
        {'database': 'SpreeDW', 'schema': 'dim', 'table': 'ValueChainDatasetFilter'},
        {'database': 'SpreeTL', 'schema': 'lookup', 'table': 'GeoCoding'},
        {'database': 'SpreeTL', 'schema': 'lookup', 'table': 'Measure'},
        {'database': 'SpreeTL', 'schema': 'lookup', 'table': 'SaleValueTypeMapping'},
        {'database': 'SpreeTL', 'schema': 'lookup', 'table': 'State'},
        {'database': 'SpreeTL', 'schema': 'lookup', 'table': 'GeoCoding'},
        {'database': 'SpreeTL', 'schema': 'lookup', 'table': 'SalesChannels'},
        {'database': 'SpreeTL', 'schema': 'etl', 'table': 'ProcessType'},
        {'database': 'SpreeTL', 'schema': 'etl', 'table': 'StateType'},
        {'database': 'SpreeTL', 'schema': 'etl', 'table': 'StatusType'}
    ]
    sql_statement = """SELECT name
FROM sys.procedures
WHERE REPLACE(REPLACE(OBJECT_DEFINITION(OBJECT_ID), '[', ''), ']', '') LIKE '%{}.{}%'"""

    current_conn_database = ""
    conn = cst.server_connect('localhost', 'master')
    staticTableReferences = {}
    for table in staticTables:
        if table['database'] != current_conn_database:
            current_conn_database = table['database']
            conn = cst.server_connect('localhost', table['database'])
        if table['database'] not in staticTableReferences:
            staticTableReferences[table['database']] = {}
        if table['schema'] not in staticTableReferences[table['database']]:
            staticTableReferences[table['database']][table['schema']] = {}
        staticTableReferences[
            table['database']
        ][
            table['schema']
        ][
            table['table']
        ] = cst.query(conn, sql_statement.format(table['schema'], table['table']))['name'].to_list()

    print(json.dumps(staticTableReferences, indent=4))
